/* Jan Rydzewski 332461 */

#ifndef LISTA_H
#define LISTA_H

#include <stdbool.h>
#include <unistd.h>

typedef struct Lista_ Lista;
typedef struct Element_ Element;

/**
 * lista dwukierunkowa
 */

Lista* lista_nowa();
void lista_usun(Lista* z);

size_t lista_daj_liczbe_elementow(Lista*);

Element* lista_dodaj_na_poczatku(void* d, Lista* z);
Element* lista_dodaj_na_koncu(void* d, Lista* z);
Element* lista_dodaj_pomiedzy(void* d, Element* l, Element* p);
Element* lista_dodaj_po(void* d, Element* e);
Element* lista_dodaj_przed(void* d, Element* e);

void lista_usun_element(Element* e);

Element* lista_daj_poczatek(Lista* z);
Element* lista_daj_koniec(Lista* z);
Element* lista_daj_nastepny(Element* e);
Element* lista_daj_poprzedni(Element* e);

void* lista_dane_elementu(Element* e);

bool lista_czy_jest_nastepny(Element* e);
bool lista_czy_jest_poprzedni(Element* e);
bool lista_czy_za_ostatnim(Element*);
bool lista_czy_przed_pierwszym(Element*);
bool lista_czy_w_liscie(Element*);


bool lista_czy_niepusta(Lista*);

void* lista_daj_dane_pierwszego_elementu_i_usun(Lista*);
void* lista_daj_dane_pierwszego_elementu(Lista*);

/* debug */
void lista_debug(Lista*);

#endif
