/* Jan Rydzewski 332461 */

#ifndef KLIENT_WYSYLACZ_H
#define KLIENT_WYSYLACZ_H

#include "klient.h"
#include "segment.h"
#include "kolejka_bajtowa.h"
#include <event2/event.h> /* libevent */
#include "datagram.h"

/**
 * KlientWysylacz to obiekt odpowiedzialny za wysyłanie właściwych danych (tych, o których przesyłanie nam chodzi).
 * KlientWysylacz przesyla do serwera dane przychodzące na deskryptor który to podaje sie mu w konstruktorze.
 */

KlientWysylacz* klient_wysylacz_nowy(Klient*, int);
void klient_wysylacz_usun(KlientWysylacz*);

bool klient_wysylacz_odbierz_dane(KlientWysylacz*, Datagram*);

#endif
