/* Jan Rydzewski 332461 */

#ifndef KLIENT_H
#define KLIENT_H

typedef struct Klient_ Klient;
typedef struct KlientPolaczenie_ KlientPolaczenie;
typedef struct KlientWysylacz_ KlientWysylacz;
typedef struct KlientOdbieracz_ KlientOdbieracz;

#include "ustawienia.h"
#include "klient_rdzen.h"
#include "klient_polaczenie.h"
#include "klient_wysylacz.h"
#include "klient_odbieracz.h"

#endif
