/* Jan Rydzewski 332461 */

#include <stdio.h> /* printf */
#include <stdbool.h> /* bool */
#include "serwer.h"
#include <stdlib.h> /* malloc */
#include <unistd.h> /* close */
#include <sys/socket.h> /* getpeername */
#include "debug.h"
#include <string.h>
#include "kolejka_bajtowa.h"
#include "protokol.h"
#include <arpa/inet.h>

enum SerwerKlientStan{
	SERWER_KLIENT_NIEPRZYWITANY,
	SERWER_KLIENT_AKTYWNY,
	SERWER_KLIENT_NAPELNIANIE
};

struct SerwerKlient_{
	Serwer* serwer;
	
	evutil_socket_t socket_TCP;
	struct sockaddr_in6 adres_UDP;
	unsigned int id;
	enum SerwerKlientStan stan;
	
	/* stan odbierania */
	unsigned int numer_oczekiwanego_segmentu;
	KolejkaBajtowa* kolejka;
	
	/* rozmiar kolejki */
	size_t maksymalny_rozmiar_kolejki;
	size_t kolejka_wysoka;
	size_t kolejka_niska;
	
	/* widziane od ostatniego raportu */
	size_t kolejka_max;
	size_t kolejka_min;
	
	struct event* event_dawno_sie_nie_odezwal;
};

void dawno_sie_nie_odzywal(SerwerKlient*);
void callback_dawno_sie_nie_odzywal(evutil_socket_t, short, void*);
bool zresetuj_timeout(SerwerKlient*);
bool wrzuc_dane(SerwerKlient*, Segment*);
size_t wolne_miejsce_w_kolejce(SerwerKlient*);

bool odbierz_UPLOAD(SerwerKlient*, Datagram*, struct sockaddr_in6*);
bool odbierz_KEEPALIVE(SerwerKlient*, Datagram*, struct sockaddr_in6*);
bool odbierz_CLIENT(SerwerKlient*, Datagram*, struct sockaddr_in6*);
bool odbierz_RETRANSMIT(SerwerKlient*, Datagram*, struct sockaddr_in6*);

bool wyslij_powitanie(SerwerKlient*);
bool wyslij_po_UDP(SerwerKlient*, Datagram*);
bool wyslij_ACK(SerwerKlient*);

SerwerKlient* serwer_klient_nowy(
	Serwer* serwer,
	evutil_socket_t socket_TCP,
	unsigned int id,
	size_t n,
	size_t kolejka_wysoka,
	size_t kolejka_niska
){
	
	spam("klient: nowy");
	
	/* alloc */
	SerwerKlient* klient = calloc(1, sizeof(SerwerKlient));
	if(klient == NULL){
		erro("klient: alloc nie poszedł");
		return NULL;
	}
	klient->socket_TCP = -1;
	
	klient->maksymalny_rozmiar_kolejki = n;
	klient->kolejka_niska = kolejka_niska;
	klient->kolejka_wysoka = kolejka_wysoka;
	
	klient->id = id;
	klient->stan = SERWER_KLIENT_NIEPRZYWITANY;
	
	
	/* rejestracja */
	klient->serwer = serwer;
	
	/* TCP */
	klient->socket_TCP = dup(socket_TCP);
	if(
		evutil_make_socket_nonblocking(klient->socket_TCP) == -1
	){
		erro("klient: sie nie udalo: TCP_socket nonblocking, reusable");
		serwer_klient_usun(klient);
		return NULL;
	}
	
	/* UDP */
	socklen_t dlugosc_adresu = sizeof(klient->adres_UDP);
	if(
		getpeername(socket_TCP, (struct sockaddr*)&(klient->adres_UDP), &dlugosc_adresu) == -1 ||
		dlugosc_adresu != sizeof(klient->adres_UDP)
	){
		erro("klient: sie nie udalo wziac adresu klienta");
		serwer_klient_usun(klient);
		return NULL;
	}
	klient->adres_UDP.sin6_port = 0;
	
	/* przywitaj sie */
	if(!wyslij_powitanie(klient)){
		erro("klient: nie udalo sie wyslac powitania");
		serwer_klient_usun(klient);
		return NULL;
	}
	
	/* event na nieaktywnosc */
	klient->event_dawno_sie_nie_odezwal = event_new(
		serwer_daj_event_base(serwer),
		-1,
		EV_TIMEOUT,
		callback_dawno_sie_nie_odzywal,
		(void*)klient
	);
	if(
		klient->event_dawno_sie_nie_odezwal == NULL ||
		!zresetuj_timeout(klient)
	){
		erro("klient: nie udalo sie zrobi timeouta");
		serwer_klient_usun(klient);
		return false;
	}
	
	/* kolejka */
	klient->kolejka = kolejka_bajtowa_nowa();
	if(klient->kolejka == NULL){
		erro("klient: nie udalo sie zrobic kolejki");
		serwer_klient_usun(klient);
		return NULL;
	}
	klient->numer_oczekiwanego_segmentu = 0;
	klient->kolejka_min = 0;
	klient->kolejka_max = 0;
	
	spam("klient: zrobiony!");
	
	return klient;
}

void serwer_klient_usun(SerwerKlient* klient){
	spam("klient: usuwanie");
	
	if(klient != NULL){
		event_free(klient->event_dawno_sie_nie_odezwal);
		close(klient->socket_TCP);
		kolejka_bajtowa_usun(klient->kolejka);
		free(klient);
	}
	
	spam("klient: usuniety");
}

char* serwer_klient_daj_raport(SerwerKlient* k){
	
	size_t r = kolejka_bajtowa_daj_rozmiar(k->kolejka);
	
	char adres[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &(k->adres_UDP.sin6_addr), adres, INET6_ADDRSTRLEN);
	
	char* c = malloc(100);
	sprintf(
		c,
		"[%s]:%hu FIFO: %zu/%zu (min. %zu, max. %zu)\n",
		adres, k->adres_UDP.sin6_port,
		r, k->maksymalny_rozmiar_kolejki,
		k->kolejka_min, k->kolejka_max
	);
	
	k->kolejka_min = r;
	k->kolejka_max = r;
	
	return c;
}

bool serwer_klient_wyslij_raport(SerwerKlient* k, const char* s){
	if(k->stan == SERWER_KLIENT_NIEPRZYWITANY){
		warn("klient: nie mozna wysylac raportow do NIEPRZYWITANYCH");
		return false;
	}
	
	ssize_t n = send(k->socket_TCP, s, strlen(s), MSG_NOSIGNAL);
	
	if(n <= 0){
		erro("klient: nie udalo sie wyslac raportu");
		serwer_klient_rozlacz_delikatnie(k);
		return false;
	}
	
	return true;
}

bool serwer_klient_wyslij_dane(SerwerKlient* k, Segment* s, unsigned int nr){
	if(k->stan == SERWER_KLIENT_NIEPRZYWITANY){
		warn("klient: nie mozna danych raportow do NIEPRZYWITANYCH");
		return false;
	}
	
	Datagram* d = protokol_zrob_DATA(
		nr,
		k->numer_oczekiwanego_segmentu,
		wolne_miejsce_w_kolejce(k),
		s
	);
	bool ok = wyslij_po_UDP(k, d);
	if(!ok){
		warn("klient: nie udalo sie wyslac DATA");
		serwer_klient_rozlacz_delikatnie(k);
	}
	datagram_usun(d);
	return ok;
}

bool serwer_klient_odbierz_dane(SerwerKlient* k, Datagram* d, struct sockaddr_in6* addr){
	return
		odbierz_CLIENT(k, d, addr) ||
		odbierz_KEEPALIVE(k, d, addr) ||
		odbierz_RETRANSMIT(k, d, addr) ||
		odbierz_UPLOAD(k, d, addr);
}

Segment* serwer_klient_daj_dane(SerwerKlient* k, size_t n){
	if(k->stan == SERWER_KLIENT_AKTYWNY){
		return kolejka_bajtowa_daj_dane(k->kolejka, n);
	}else if(k->stan == SERWER_KLIENT_NAPELNIANIE){
		return segment_nowy(NULL, 0);
	}else{
		warn("klient: czytanie od klienta w stanie NAPELNIANIE");
		return NULL;
	}
}

bool serwer_klient_usun_dane(SerwerKlient* k, size_t n){
	return kolejka_bajtowa_usun_dane(k->kolejka, n) == n;
}

void serwer_klient_rozlacz_delikatnie(SerwerKlient* k){
	event_active(
		k->event_dawno_sie_nie_odezwal,
		EV_TIMEOUT,
		0
	);
}

/* prywatne */

bool wrzuc_dane(SerwerKlient* k, Segment* s){
	if(k->stan == SERWER_KLIENT_NIEPRZYWITANY){
		warn("klient: nie mozna wrzucac danych do nieprzywitanego klienta");
		return false;
	}
	
	if(segment_daj_rozmiar(s) > wolne_miejsce_w_kolejce(k)){
		warn("klient: klient wychodzi ponad okno");
		return false;
	}
	
	if(!kolejka_bajtowa_wrzuc_segment(k->kolejka, s)){
		warn("klient: nie udalo sie wrzucic segmentu");
		return false;
	}
	
	size_t r = kolejka_bajtowa_daj_rozmiar(k->kolejka);
	if(r > k->kolejka_max){
		k->kolejka_max = r;
	}else if(r < k->kolejka_min){
		k->kolejka_min = r;
	}
	
	if(r <= k->kolejka_niska){
		k->stan = SERWER_KLIENT_NAPELNIANIE;
	}else if(r >= k->kolejka_wysoka){
		k->stan = SERWER_KLIENT_AKTYWNY;
	}
	
	return true;
}

size_t wolne_miejsce_w_kolejce(SerwerKlient* k){
	return k->maksymalny_rozmiar_kolejki - kolejka_bajtowa_daj_rozmiar(k->kolejka);
}

bool odbierz_UPLOAD(SerwerKlient* k, Datagram* d, struct sockaddr_in6* addr){
	unsigned int nr;
	
	if(
		memcmp(&(addr->sin6_addr), &(k->adres_UDP.sin6_addr), sizeof(struct in6_addr)) == 0 &&
		k->adres_UDP.sin6_port == addr->sin6_port &&
		protokol_parsuj_UPLOAD(d, &nr)
	){
		if(k->stan == SERWER_KLIENT_AKTYWNY || k->stan == SERWER_KLIENT_NAPELNIANIE){
			
			zresetuj_timeout(k);
			
			if(nr == k->numer_oczekiwanego_segmentu){
				wrzuc_dane(k, datagram_daj_segment(d));
				k->numer_oczekiwanego_segmentu++;
				if(!wyslij_ACK(k)){
					warn("klient: nie udalo sie wyslac ACK po UPLOAD'zie %u", k->numer_oczekiwanego_segmentu - 1);
				}
				return true;
			}else{
				spam("klient: klient przysłał UPLOAD od czapy (%u, a spodziewalem sie %u)", nr, k->numer_oczekiwanego_segmentu);
				return false;
			}
			
		}else{
			spam("klient: klient przysłał UPLOAD mimo ze nie jest aktywny lub napelniany");
			return false;
		}
		
	}else{
		return false;
	}
}

bool odbierz_KEEPALIVE(SerwerKlient* k, Datagram* d, struct sockaddr_in6* addr){
	if(
		memcmp(&(addr->sin6_addr), &(k->adres_UDP.sin6_addr), sizeof(struct in6_addr)) == 0 &&
		k->adres_UDP.sin6_port == addr->sin6_port &&
		protokol_parsuj_KEEPALIVE(d)
	){
		if(k->stan == SERWER_KLIENT_AKTYWNY || k->stan == SERWER_KLIENT_NAPELNIANIE){
			zresetuj_timeout(k);
			return true;
		}else{
			warn("klient: klient przysłał KEEPALIVE mimo ze nie jest aktywny lub napelniany");
			return false;
		}
	}else{
		return false;
	}
}

bool odbierz_CLIENT(SerwerKlient* k, Datagram* d, struct sockaddr_in6* addr){
	unsigned int id;
	if(
		memcmp(&(addr->sin6_addr), &(k->adres_UDP.sin6_addr), sizeof(struct in6_addr)) == 0 &&
		protokol_parsuj_CLIENT(d, &id) &&
		id == k->id
	){
		if(k->stan == SERWER_KLIENT_NIEPRZYWITANY){
			zresetuj_timeout(k);
			
			if(k->adres_UDP.sin6_port != 0){
				warn("klient: NIEPRZYWITANY a ma juz numer portu");
			}
			
			k->adres_UDP.sin6_port = addr->sin6_port;
			k->stan = SERWER_KLIENT_NAPELNIANIE;
			
			spam("klient: klient %u sie przywital", id);
			
			return true;
		}else{
			warn("klient: klient przyslal CLIENT a nie jest w stanie NIEPRZYWITANY");
			return false;
		}
	}else{
		return false;
	}
}

bool odbierz_RETRANSMIT(SerwerKlient* k, Datagram* d, struct sockaddr_in6* addr){
	unsigned int nr;
	
	if(
		memcmp(&(addr->sin6_addr), &(k->adres_UDP.sin6_addr), sizeof(struct in6_addr)) == 0 &&
		k->adres_UDP.sin6_port == addr->sin6_port &&
		protokol_parsuj_RETRANSMIT(d, &nr)
	){
		if(k->stan == SERWER_KLIENT_AKTYWNY || k->stan == SERWER_KLIENT_NAPELNIANIE){
			zresetuj_timeout(k);
			
			serwer_wysylacz_retransmituj(
				serwer_pozycz_wysylacza(k->serwer),
				k,
				nr
			);
			
			return true;
		}else{
			warn("klient: klient przysłał RETRANSMIT mimo ze nie jest aktywny lub napelniany");
			return false;
		}
	}else{
		return false;
	}
}

bool wyslij_ACK(SerwerKlient* k){
	Datagram* d = protokol_zrob_ACK(
		k->numer_oczekiwanego_segmentu,
		wolne_miejsce_w_kolejce(k)
	);
	bool ok = wyslij_po_UDP(k ,d);
	if(!ok){
		warn("klient: nie udalo sie wsylac ACK");
	}
	datagram_usun(d);
	return ok;
}

bool wyslij_po_UDP(SerwerKlient* k, Datagram* d){
	if(k == NULL || d == NULL){
		warn("klient: serwer_klient_wyslij_po_UDP(%p, %p)", (void*)k, (void*)d);
		return false;
	}
	return serwer_UDP_wyslij(serwer_pozycz_UDP(k->serwer), d, &(k->adres_UDP));
}

bool wyslij_powitanie(SerwerKlient* klient){
	char dane[30];
	int n = sprintf(dane, "CLIENT %d\n", klient->id);
	if(n < 0){
		erro("klient: sprintf w powitaniu?");
		return false;
	}
	
	if(write(klient->socket_TCP, dane, n) != n){
		erro("klient: nie udalo sie wyslac powitania");
		return false;
	}
	
	return true;
}

void dawno_sie_nie_odzywal(SerwerKlient* k){
	if(k == NULL){
		warn("serwer_klient.c: dawno_sie_nie_odzywal(%p)", (void*)k);
		return;
	}
	info("klient: klient %u dawno sie nie odzywal, wywalam", k->id);
	serwer_usun_klienta(k->serwer, k);
}

bool zresetuj_timeout(SerwerKlient* k){
	event_del(k->event_dawno_sie_nie_odezwal);
	struct timeval interval = { 0, (1000*1000)*TIMEOUT_ROZLACZENIA };
	if(
		event_add(k->event_dawno_sie_nie_odezwal, &interval) == -1
	){
		warn("serwer: nie dalo sie zresetowac timeouta");
		return false;
	}
	return true;
	
}

void callback_dawno_sie_nie_odzywal(evutil_socket_t fd, short what, void* arg){
	(void)fd;
	(void)what;
	dawno_sie_nie_odzywal((SerwerKlient*)arg);
}
