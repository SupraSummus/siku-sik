/* Jan Rydzewski 332461 */

#include "segment.h"
#include <stdlib.h>
#include <string.h>
#include "debug.h"
#include <errno.h>

struct Segment_{
	void* dane;
	size_t rozmiar;
};

Segment* segment_nowy(void* dane, size_t rozmiar){
	Segment* s = calloc(1, sizeof(Segment));
	if(s == NULL){
		return NULL;
	}
	
	s->rozmiar = rozmiar;
	
	s->dane = malloc(rozmiar);
	if(s->dane == NULL && rozmiar > 0){
		segment_usun(s);
		return NULL;
	}
	memcpy(s->dane, dane, rozmiar);
	
	return s;
}

Segment* segment_kopiuj(Segment* s){
	return segment_nowy(s->dane, s->rozmiar);
}

void segment_usun(Segment* s){
	if(s == NULL){
		return;
	}
	free(s->dane);
	free(s);
}

bool segment_dopisz(Segment* a, Segment* b){
	void* nowe = realloc(a->dane, a->rozmiar + b->rozmiar);
	if(nowe == NULL && a->rozmiar + b->rozmiar > 0){
		return false;
	}
	a->dane = nowe;
	
	memcpy((char*)a->dane + a->rozmiar, b->dane, b->rozmiar);
	a->rozmiar += b->rozmiar;
	
	return true;
}

Segment* segment_utnij_poczatek(Segment* s, size_t n){
	if(n > s->rozmiar){
		n = s->rozmiar;
	}
	
	Segment* ss = segment_nowy(s->dane, n);
	if(ss == NULL){
		return NULL;
	}
	
	s->rozmiar -= n;
	memmove(s->dane, (char*)s->dane + n, s->rozmiar);
	
	return ss;
}

bool segment_wypelnij(Segment* s, void* dane, size_t rozmiar){
	if(s == NULL){
		return false;
	}
	
	free(s->dane);
	
	s->rozmiar = rozmiar;
	
	s->dane = malloc(rozmiar);
	if(s->dane == NULL && rozmiar > 0){
		segment_usun(s);
		return false;
	}
	memcpy(s->dane, dane, rozmiar);
	
	return true;
}

bool segment_wypisz_sie(Segment* s, int fd){
	ssize_t n = write(fd, s->dane, s->rozmiar);
	return n >= 0 && (size_t)n == s->rozmiar;
}

bool segment_wyslij(Segment* s, int fd){
	if(s == NULL || fd < 0){
		warn("segment_wyslij(%p, %i)", (void*)s, fd);
		return false;
	}
	
	ssize_t n = send(
		fd,
		s->dane,
		s->rozmiar,
		0
	);
	
	if(n < 0 || (size_t)n != s->rozmiar){
		warn("segment_wyslij(%p, %i): %s", (void*)s, fd, strerror(errno));
		return false;
	}
	
	return true;
}

bool segment_wyslij_do_ipv6(Segment* s, int fd, struct sockaddr_in6* addr){
	if(s == NULL || fd < 0 || addr == NULL){
		warn("segment_wyslij_do_ipv6(%p, %i, %p)", (void*)s, fd, (void*)addr);
		return false;
	}
	
	ssize_t n = sendto(
		fd,
		s->dane,
		s->rozmiar,
		0,
		(struct sockaddr*)addr,
		sizeof(*addr)
	);
	
	if(n < 0 || (size_t)n != s->rozmiar){
		warn("segment_wyslij_do_ipv6(%p, %i, %p): %s", (void*)s, fd, (void*)addr, strerror(errno));
	}
	
	return n >= 0 && (size_t)n == s->rozmiar;
}

void* segment_pozycz_dane(Segment* s){
	if(s == NULL){
		warn("segment_pozycz_dane(%p)", (void*)s);
		return 0;
	}
	return s->dane;
}

size_t segment_daj_rozmiar(Segment* s){
	if(s == NULL){
		warn("segment_daj_rozmiar(%p)", (void*)s);
		return 0;
	}
	return s->rozmiar;
}
