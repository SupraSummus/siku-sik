/* Jan Rydzewski 332461 */

#ifndef SERWER_MIKSER_H
#define SERWER_MIKSER_H

#include <stdbool.h>
#include "serwer.h"

/**
 * Mikser zajmuje sie miksowaniem. Ma w sobie timer, dzieki któremu miksuje regularnie. Zmiksowane rzeczy przekazuje do wysylacza.
 */

SerwerMikser* serwer_mikser_nowy(Serwer*, unsigned int);
void serwer_mikser_usun(SerwerMikser*);

#endif
