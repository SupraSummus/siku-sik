/* Jan Rydzewski 332461 */

#include "lista.h"
#include <stdlib.h> /* malloc */
#include <stdio.h>

struct Lista_{
	Element* l;
	Element* p;
	size_t liczba_elementow;
};

struct Element_{
	Element* l;
	Element* p;
	Lista* lista;
	void* dane;
};

Lista* lista_nowa(){
	Lista* z = malloc(sizeof(Lista));
	Element* l = malloc(sizeof(Element));
	Element* p = malloc(sizeof(Element));
	
	if(z == NULL || l == NULL || p == NULL){
		free(z);
		free(l);
		free(p);
		return NULL;
	}
	
	l->dane = NULL;
	l->lista = z;
	l->l = NULL;
	l->p = p;
	
	p->dane = NULL;
	p->lista = z;
	p->p = NULL;
	p->l = l;
	
	z->l = l;
	z->p = p;
	z->liczba_elementow = 0;
	
	return z;
}

void lista_usun(Lista* z){
	if(z == NULL){
		return;
	}
	
	while(lista_czy_niepusta(z)){
		lista_usun_element(lista_daj_poczatek(z));
	}
	
	free(z->l);
	free(z->p);
	free(z);
}

size_t lista_daj_liczbe_elementow(Lista* l){
	if(l == NULL){
		return 0;
	}
	
	return l->liczba_elementow;
}

Element* lista_dodaj_na_poczatku(void* d, Lista* z){
	if(z == NULL){
		return NULL;
	}
	return lista_dodaj_po(d, z->l);
}

Element* lista_dodaj_na_koncu(void* d, Lista* z){
	if(z == NULL){
		return NULL;
	}
	return lista_dodaj_przed(d, z->p);
}

Element* lista_dodaj_pomiedzy(void* d, Element* l, Element* p){
	if(
		l == NULL ||
		p == NULL ||
		l->p != p ||
		p->l != l
	){
		return NULL;
	}
	
	Element* e = malloc(sizeof(Element));
	if(e == NULL){
		return NULL;
	}
	
	e->dane = d;
	e->lista = l->lista;
	e->l = l;
	e->p = p;
	
	p->l = e;
	l->p = e;
	
	e->lista->liczba_elementow++;
	
	return e;
}

Element* lista_dodaj_po(void* d, Element* l){
	if(l == NULL){
		return NULL;
	}
	return lista_dodaj_pomiedzy(d, l, l->p);
}

Element* lista_dodaj_przed(void* d, Element* p){
	if(p == NULL){
		return NULL;
	}
	return lista_dodaj_pomiedzy(d, p->l, p);
}

void lista_usun_element(Element* e){
	if(e == NULL){
		return;
	}
	
	Element* l = e->l;
	Element* p = e->p;
	
	if(l != NULL){ 
		l->p = p;
	}
	if(p != NULL){
		p->l = l;
	}
	
	e->lista->liczba_elementow--;
	
	free(e);
}

Element* lista_daj_poczatek(Lista* z){
	if(z == NULL){
		return NULL;
	}
	return lista_daj_nastepny(z->l);
}

Element* lista_daj_koniec(Lista* z){
	if(z == NULL){
		return NULL;
	}
	return lista_daj_poprzedni(z->p);
}

Element* lista_daj_poprzedni(Element* e){
	if(e == NULL){
		return NULL;
	}else if(e->l == NULL){
		return NULL;
	}else if(e->l->l == NULL){
		return NULL;
	}else{
		return e->l;
	}
}

Element* lista_daj_nastepny(Element* e){
	if(e == NULL){
		return NULL;
	}else if(e->p == NULL){
		return NULL;
	}else if(e->p->p == NULL){
		return NULL;
	}else{
		return e->p;
	}
}

void* lista_dane_elementu(Element* e){
	if(e == NULL){
		return NULL;
	}
	return e->dane;
}

bool lista_czy_jest_nastepny(Element* e){
	if(e == NULL){
		return false;
	}
	return lista_daj_nastepny(e) != NULL;
}

bool lista_czy_jest_poprzedni(Element* e){
	if(e == NULL){
		return false;
	}
	return lista_daj_poprzedni(e) != NULL;
}

bool lista_czy_za_ostatnim(Element* e){
	if(e == NULL){
		return false;
	}
	return e->p == NULL;
}

bool lista_czy_przed_pierwszym(Element* e){
	if(e == NULL){
		return false;
	}
	return e->l == NULL;
}

bool lista_czy_w_liscie(Element* e){
	if(e == NULL){
		return false;
	}
	return e->l != NULL && e->p != NULL;
}

bool lista_czy_niepusta(Lista* l){
	if(l == NULL){
		return false;
	}
	
	return l->l->p != l->p;
}

void* lista_daj_dane_pierwszego_elementu_i_usun(Lista* l){
	Element* e = lista_daj_poczatek(l);
	void* dane = lista_dane_elementu(e);
	lista_usun_element(e);
	return dane;
}

void* lista_daj_dane_pierwszego_elementu(Lista* l){
	return lista_dane_elementu(lista_daj_poczatek(l));
}

void lista_debug(Lista* l){
	fprintf(stderr, "lista debug:\n");
	fprintf(stderr, "%p | %p\n", (void*)l->l, (void*)l->p);
	
	for(
		Element* i = l->l;
		i != NULL;
		i = i->p
	){
		fprintf(stderr, "%p < %p > %p\n", (void*)i->l, (void*)i, (void*)i->p);
	}
}
