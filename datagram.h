/* Jan Rydzewski 332461 */

#ifndef DATAGRAM_H
#define DATAGRAM_H

#include <stdbool.h>
#include <unistd.h>
#include "segment.h"
#include <sys/socket.h>
#include <netinet/in.h>

/**
 * datagram ma naglowek i dane
 */

typedef struct Datagram_ Datagram;

Datagram* datagram_nowy(const char*, void*, size_t);
Datagram* datagram_nowy_printf(void*, size_t, const char*, ...);
Datagram* datagram_nowy_segment_printf(Segment*, const char*, ...);

Datagram* datagram_odbierz(int);
Datagram* datagram_odbierz_od_ipv6(int, struct sockaddr_in6*);

bool datagram_wyslij(Datagram*, int);
bool datagram_wyslij_do_ipv6(Datagram*, int, struct sockaddr_in6*);

int datagram_scanf(Datagram*, const char*, ...);

char* datagram_pozycz_naglowek(Datagram*);
Segment* datagram_daj_segment(Datagram*); /* TODO przenazwac to na pozycz */
Segment* datagram_jako_segment(Datagram*);


void datagram_usun(Datagram*);

#endif
