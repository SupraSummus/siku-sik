/* Jan Rydzewski 332461 */

#ifndef SERWER_LACZYCIEL_H
#define SERWER_LACZYCIEL_H

#include <stdbool.h>
#include "serwer.h"

/**
 * obiekt utrzymujacy słuchający deskryptor.
 * robi klienta gdy ktos sie do tego deskryptora podłącza.
 */

SerwerLaczyciel* serwer_laczyciel_nowy(Serwer*, unsigned short int);
void serwer_laczyciel_usun(SerwerLaczyciel*);


#endif
