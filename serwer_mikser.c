/* Jan Rydzewski 332461 */

#include "serwer_mikser.h"
#include "debug.h"
#include <stdlib.h>
#include "mixer.h"

struct SerwerMikser_{
	Serwer* serwer;
	unsigned int tx_interval;
	struct event* timer;
};

void callback_miksuj(evutil_socket_t, short, void*);
void miksuj(SerwerMikser*);

SerwerMikser* serwer_mikser_nowy(Serwer* s, unsigned int tx_interval){
	spam("mikser: robienie nowego");
	
	/* alloc */
	SerwerMikser* m = calloc(1, sizeof(SerwerMikser));
	if(m == NULL){
		erro("mikser: walnął alloc");
		return NULL;
	}
	
	m->tx_interval = tx_interval;
	
	/* ojciec */
	m->serwer = s;
	
	/* timer */
	m->timer = event_new(
		serwer_daj_event_base(s),
		-1,
		EV_PERSIST,
		callback_miksuj,
		(void*)m
	);
	struct timeval interval = { 0, 1000*tx_interval };
	if(
		m->timer == NULL ||
		event_add(m->timer, &interval) == -1
	){
		erro("mikser: nie udalo sie zrobic timera");
		serwer_mikser_usun(m);
		return NULL;
	}
	
	spam("mikser: zrobiony");
	
	return m;
}

void serwer_mikser_usun(SerwerMikser* m){
	spam("mikser: usuwanie");
	
	if(m != NULL){
		event_free(m->timer);
		free(m);
	}
	
	spam("mikser: usuniety");
}

/* prywatne */

void callback_miksuj(evutil_socket_t fd, short what, void* arg){
	(void)fd;
	(void)what;
	miksuj((SerwerMikser*)arg);
}

void miksuj(SerwerMikser* m){
	size_t liczba_klientow = lista_daj_liczbe_elementow(serwer_daj_liste_klientow(m->serwer));
	
	struct mixer_input* wejscie = calloc(liczba_klientow, sizeof(struct mixer_input));
	SerwerKlient** klienci = calloc(liczba_klientow, sizeof(SerwerKlient*));
	Segment** segmenty = calloc(liczba_klientow, sizeof(Segment*));
	size_t n = 0;
	
	void* wyjscie = malloc(MAKSYMALNY_ROZMIAR_WYSYLANEGO_SEGMENTU);
	size_t rozmiar_wyjscia = MAKSYMALNY_ROZMIAR_WYSYLANEGO_SEGMENTU;
	
	if(wejscie == NULL || wyjscie == NULL || klienci == NULL || segmenty == NULL){
		warn("mikser: miksuj(): nie udal sie alloc");
	}else{
	
		/* zrob wejscie */
		for(
			Element* e = lista_daj_poczatek(serwer_daj_liste_klientow(m->serwer));
			lista_czy_w_liscie(e);
			e = lista_daj_nastepny(e)
		){
			SerwerKlient* k = (SerwerKlient*)lista_dane_elementu(e);
			Segment* s = serwer_klient_daj_dane(k, MAKSYMALNY_ROZMIAR_WYSYLANEGO_SEGMENTU);
			if(s != NULL && segment_daj_rozmiar(s) > 0){
				wejscie[n].data = segment_pozycz_dane(s);
				wejscie[n].len = segment_daj_rozmiar(s);
				wejscie[n].consumed = 0;
				klienci[n] = k;
				segmenty[n] = s;
				n++;
			}else{
				segment_usun(s);
			}
		}
		
		mixer(
			wejscie, n,
			wyjscie, &rozmiar_wyjscia,
			m->tx_interval
		);
		
		/* usun dane z wejscia */
		for(size_t i = 0; i < n; i++){
			if(!serwer_klient_usun_dane(klienci[i], wejscie[i].consumed)){
				warn("mikser: nie udalo sie usunac %zu bajtow z kolejki klienta", wejscie[i].consumed);
			}
			segment_usun(segmenty[i]);
		}
		
		/* wyslij output */
		Segment* s = segment_nowy(wyjscie, rozmiar_wyjscia);
		if(!serwer_wysylacz_wyslij(serwer_pozycz_wysylacza(m->serwer), s)){
			spam("mikser: nie udalo sie wyslac segmentu");
		}
		segment_usun(s);
		
	}
	
	free(wejscie);
	free(wyjscie);
	free(klienci);
	free(segmenty);
}
