/* Jan Rydzewski 332461 */

#ifndef KOLEJKA_BAJTOWA_H
#define KOLEJKA_BAJTOWA_H

#include <unistd.h>
#include <stdbool.h>
#include "segment.h"

/**
 * KolejkaBajtowa kolejkuje bajty.
 */

typedef struct KolejkaBajtowa_ KolejkaBajtowa;

KolejkaBajtowa* kolejka_bajtowa_nowa();
void kolejka_bajtowa_usun(KolejkaBajtowa*);

/* operacje segmentowe */
bool kolejka_bajtowa_wrzuc_segment(KolejkaBajtowa*, Segment*);

Segment* kolejka_bajtowa_daj_segment(KolejkaBajtowa*);
Segment* kolejka_bajtowa_daj_i_usun_segment(KolejkaBajtowa*);

bool kolejka_bajtowa_usun_segment(KolejkaBajtowa*);
size_t kolejka_bajtowa_daj_liczbe_segmentow(KolejkaBajtowa*);

/* operacje bajtowe */
bool kolejka_bajtowa_wrzuc_dane(KolejkaBajtowa*, Segment*);

Segment* kolejka_bajtowa_daj_dane(KolejkaBajtowa*, size_t);
Segment* kolejka_bajtowa_daj_i_usun_dane(KolejkaBajtowa*, size_t);

size_t kolejka_bajtowa_usun_dane(KolejkaBajtowa*, size_t);
size_t kolejka_bajtowa_daj_rozmiar(KolejkaBajtowa*);

/* debug */
bool kolejka_bajtowa_wypisz_sie(KolejkaBajtowa*, int);
#endif
