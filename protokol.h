/* Jan Rydzewski 332461 */

#ifndef PROTOKOL_H
#define PROTOKOL_H

#include "datagram.h"
#include "segment.h"
#include <stdbool.h>

/**
 * zestaw funkcji do robienia i czytania datagramow zgodnych z protokolem
 */

/* CLIENT */
bool protokol_parsuj_CLIENT(Datagram*, unsigned int*);
Datagram* protokol_zrob_CLIENT(unsigned int);

/* UPLOAD */
bool protokol_parsuj_UPLOAD(Datagram*, unsigned int*);
Datagram* protokol_zrob_UPLOAD(unsigned int, Segment*);

/* DATA */
bool protokol_parsuj_DATA(Datagram*, unsigned int*, unsigned int*, unsigned int*);
Datagram* protokol_zrob_DATA(unsigned int, unsigned int, unsigned int, Segment*);

/* ACK */
bool protokol_parsuj_ACK(Datagram*, unsigned int*, unsigned int*);
Datagram* protokol_zrob_ACK(unsigned int, unsigned int);

/* RETRANSMIT */
bool protokol_parsuj_RETRANSMIT(Datagram*, unsigned int*);
Datagram* protokol_zrob_RETRANSMIT(unsigned int);

/* KEEPALIVE */
bool protokol_parsuj_KEEPALIVE(Datagram*);
Datagram* protokol_zrob_KEEPALIVE();

#endif
