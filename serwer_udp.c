/* Jan Rydzewski 332461 */

#include "serwer_udp.h"
#include <stdlib.h>
#include "debug.h"
#include <string.h>

struct SerwerUDP_{
	Serwer* serwer;
	evutil_socket_t socket_UDP;
	struct event* event_sa_dane;
};

void odbierz_dane(SerwerUDP*);
void callback_odbierz_dane(evutil_socket_t, short, void*);

SerwerUDP* serwer_UDP_nowy(Serwer* serwer, unsigned short port){
	spam("odbieracz: robienie");
	
	/* alloc */
	SerwerUDP* odbieracz = calloc(1, sizeof(SerwerUDP));
	if(odbieracz == NULL){
		erro("odbieracz: nie udalo sie zaalokowac");
		return NULL;
	}
	
	/* ojciec */
	odbieracz->serwer = serwer;
	
	/* socket UDP */
	todo("serwer_laczyciel_nowy(): sprawdzic trzeci parametr socket()");
	if(
		(odbieracz->socket_UDP = socket(AF_INET6, SOCK_DGRAM, 0)) == -1 ||
		evutil_make_socket_nonblocking(odbieracz->socket_UDP) == -1
	){
		erro("odbieracz: walnęło robienie socketu UDP");
		serwer_UDP_usun(odbieracz);
		return NULL;
	}
	
	/* konfiguracja socketu UDP */
	struct sockaddr_in6 adres;
	memset(&adres, 0, sizeof(adres));
	adres.sin6_family = AF_INET6;
	adres.sin6_port = htons(port);
	adres.sin6_addr = in6addr_any;
	
	if(
		bind(
			odbieracz->socket_UDP,
			(struct sockaddr *) &adres,
			sizeof(adres)
		) < 0
	){
		erro("odbieracz: nie da sie zbindować socketu UDP");
		serwer_UDP_usun(odbieracz);
		return NULL;
	}
	
	/* rejestracja obsługi */
	odbieracz->event_sa_dane = event_new(
		serwer_daj_event_base(serwer),
		odbieracz->socket_UDP,
		EV_READ|EV_PERSIST,
		callback_odbierz_dane,
		(void*)odbieracz
	);
	if(
		odbieracz->event_sa_dane == NULL ||
		event_add(odbieracz->event_sa_dane, NULL) == -1
	){
		erro("odbieracz: nie mozna zrobic eventa");
		serwer_UDP_usun(odbieracz);
		return NULL;
	}
	
	spam("odbieracz: zrobiony");
	
	return odbieracz;
}

void serwer_UDP_usun(SerwerUDP* odbieracz){
	spam("odbieracz: usuwanie");
	
	if(odbieracz != NULL){
		event_free(odbieracz->event_sa_dane);
		close(odbieracz->socket_UDP);
		free(odbieracz);
	}
	
	spam("odbieracz: usuniety");
}

bool serwer_UDP_wyslij(SerwerUDP* udp, Datagram* d, struct sockaddr_in6* addr){
	if(udp == NULL || d == NULL || addr == NULL){
		warn("UDP: serwer_UDP_wyslij(%p, %p, %p)", (void*)udp, (void*)d, (void*)addr);
		return false;
	}
	return datagram_wyslij_do_ipv6(
		d,
		udp->socket_UDP,
		addr
	);
}

/* prywatne */

void odbierz_dane(SerwerUDP* odbieracz){
	struct sockaddr_in6 adres;
	Datagram* d = datagram_odbierz_od_ipv6(
		odbieracz->socket_UDP,
		&adres
	);
	if(d == NULL){
		erro("UDP: nie udalo sie odczytac datagramu");
	}
	
	bool wziety = false;
	for(
		Element* e = lista_daj_poczatek(serwer_daj_liste_klientow(odbieracz->serwer));
		lista_czy_w_liscie(e);
		e = lista_daj_nastepny(e)
	){
		SerwerKlient* klient = (SerwerKlient*)lista_dane_elementu(e);
		if(serwer_klient_odbierz_dane(klient, d, &adres)){
			if(wziety){
				warn("UDP: datagram zostal przyjety wiecej niz raz:");
				warn("UDP: <%s>", datagram_pozycz_naglowek(d));
			}
			wziety = true;
		}
	}
	
	if(!wziety){
		spam("UDP: nikt nie chciał datagramu:");
		spam("UDP: <%s>", datagram_pozycz_naglowek(d));
	}
	
	datagram_usun(d);
}

void callback_odbierz_dane(evutil_socket_t fd, short what, void* arg){
	(void)fd;
	(void)what;
	odbierz_dane((SerwerUDP*)arg);
}
