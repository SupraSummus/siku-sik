/* Jan Rydzewski 332461 */

#include "klient_wysylacz.h"
#include <stdlib.h>
#include "debug.h"
#include "protokol.h"
#include <errno.h>
#include <string.h>

struct KlientWysylacz_{
	Klient* klient;
	
	/* czytanie danych do wyslania */
	int zrodlo;
	
	/* stan strumienia */
	unsigned int numer_nastepnego_segmentu; /* ktory bysmy chcieli wyslac */
	Segment* ostatnio_wyslany_segment;
	unsigned int liczba_DATA_z_niepotwierdzonym_UPLOAD;
	unsigned int numer_oczekiwanego_segmentu; /* oczekiwanego przez serwer */
	size_t okno;
};

bool wyslij_ile_wlezie(KlientWysylacz*);
Segment* daj_dane(KlientWysylacz*);

KlientWysylacz* klient_wysylacz_nowy(Klient* klient, int zrodlo){
	spam("wysylacz: robienie");
	
	/* alloc */
	KlientWysylacz* wysylacz = calloc(1, sizeof(KlientWysylacz));
	if(wysylacz == NULL){
		erro("wysylacz: alloc walnął");
		return NULL;
	}
	
	/* ojciec */
	wysylacz->klient = klient;
	
	/* zrodlo */
	wysylacz->zrodlo = dup(zrodlo);
	evutil_make_socket_nonblocking(wysylacz->zrodlo);
	
	/* stan polaczenia */
	wysylacz->numer_nastepnego_segmentu = 0;
	wysylacz->numer_oczekiwanego_segmentu = 0;
	wysylacz->okno = 0;
	wysylacz->ostatnio_wyslany_segment = NULL;
	wysylacz->liczba_DATA_z_niepotwierdzonym_UPLOAD = 0;
	
	spam("wysylacz: zrobiony");
	
	return wysylacz;
}

void klient_wysylacz_usun(KlientWysylacz* wysylacz){
	spam("wysylacz: usuwanie");
	
	if(wysylacz != NULL){
		segment_usun(wysylacz->ostatnio_wyslany_segment);
		close(wysylacz->zrodlo);
		free(wysylacz);
	}
	
	spam("wysylacz: usuniety");
}

bool klient_wysylacz_odbierz_dane(KlientWysylacz* w, Datagram* d){
	unsigned int nr, ack, win;
	
	if(
		protokol_parsuj_ACK(d, &ack, &win) ||
		protokol_parsuj_DATA(d, &nr, &ack, &win)
	){
		if(ack < w->numer_oczekiwanego_segmentu){
			spam("wysylacz: klient zmniejszyl wartosc ACK");
			return true;
		}
		
		if(w->numer_nastepnego_segmentu <= ack){
			w->liczba_DATA_z_niepotwierdzonym_UPLOAD = 0;
			w->okno = win;
			w->numer_oczekiwanego_segmentu = ack;
		}else{
			w->liczba_DATA_z_niepotwierdzonym_UPLOAD++;
		}
		
		if(!wyslij_ile_wlezie(w)){
			warn("wysylacz: wyslanie sie nie powiodlo");
		}
		
		return true;
		
	}else{
		return false;
	}
}

/* prywatne */

bool wyslij_ile_wlezie(KlientWysylacz* wysylacz){
	bool ret = true;
	
	if(
		wysylacz->numer_nastepnego_segmentu <= wysylacz->numer_oczekiwanego_segmentu &&
		wysylacz->okno > 0 &&
		wysylacz->zrodlo >= 0
	){
		/* wysyłamy normalnie */
		
		Segment* s = daj_dane(wysylacz);
		
		if(s != NULL && segment_daj_rozmiar(s) > 0){
			Datagram* d = protokol_zrob_UPLOAD(
				wysylacz->numer_nastepnego_segmentu,
				s
			);
			
			if(
				!klient_polaczenie_wyslij_datagram(klient_daj_polaczenie(wysylacz->klient), d)
			){
				warn("wysylacz: nie udalo sie wyslac UPLOAD %u", wysylacz->numer_nastepnego_segmentu);
				ret = false;
			}
			
			datagram_usun(d);
			
			wysylacz->okno -= segment_daj_rozmiar(s);
			wysylacz->numer_nastepnego_segmentu++;
			
			segment_usun(wysylacz->ostatnio_wyslany_segment);
			wysylacz->ostatnio_wyslany_segment = segment_kopiuj(s);
		}
		
		segment_usun(s);
		
	}else if(
		wysylacz->numer_nastepnego_segmentu > wysylacz->numer_oczekiwanego_segmentu &&
		wysylacz->okno > 0 &&
		wysylacz->liczba_DATA_z_niepotwierdzonym_UPLOAD >= 2
	){
		
		/* serwer uparcie nie wysyla ACK */
		
		if(wysylacz->numer_nastepnego_segmentu == 0){
			warn("wysylacz: retransmisja na początku? beee");
			return false;
		}
		
		spam("wysylacz: retransmituję (DATA %u)", wysylacz->numer_nastepnego_segmentu - 1);
		
		if(wysylacz->ostatnio_wyslany_segment == NULL){
			warn("wysylacz: nie mam co retransmitowac bo poprzedni segment to NULL");
			return false;
		}
		
		Datagram* d = protokol_zrob_UPLOAD(
			wysylacz->numer_nastepnego_segmentu - 1,
			wysylacz->ostatnio_wyslany_segment
		);
		
		if(
			!klient_polaczenie_wyslij_datagram(klient_daj_polaczenie(wysylacz->klient), d)
		){
			warn("wysylacz: nie udalo sie wyslac UPLOAD %u", wysylacz->numer_nastepnego_segmentu - 1);
			ret = false;
		}
		
		datagram_usun(d);
		
	}
	
	return ret;
}

Segment* daj_dane(KlientWysylacz* wysylacz){
	size_t rozmiar = MAKSYMALNY_ROZMIAR_WYSYLANEGO_SEGMENTU;
	if(rozmiar > wysylacz->okno){
		rozmiar = wysylacz->okno;
	}
	
	char* bufor = malloc(rozmiar);
	if(bufor == NULL){
		warn("wysylacz: nie udalo sie zaalokowac bufora");
		return NULL;
	}
	
	ssize_t wczytane = read(wysylacz->zrodlo, (void*)bufor, rozmiar);
	
	Segment* s = NULL;
	
	if(wczytane == 0){
		
		info("wysylacz: koniec STDIN");
		
		close(wysylacz->zrodlo);
		wysylacz->zrodlo = -1;
		
		s = segment_nowy(NULL, 0);
		
	}else if(wczytane == -1){
		
		if(errno == EAGAIN || errno == EWOULDBLOCK){
			s = segment_nowy(NULL, 0);
		}else{
			erro("wysylacz: blad czytania z STDIN: %s", strerror(errno));
		}
		
	}else{
		
		s = segment_nowy((void*)bufor, (size_t)wczytane);
	}
	
	free(bufor);
	return s;
}
