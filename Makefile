CC           := gcc
CDEBUGFLAGS  := -Wall -Wextra --pedantic -std=gnu99 -c -ggdb3 -D _DEBUG -O0
LDDEBUGFLAGS := -Wall -Wextra --pedantic -std=gnu99 -ggdb3 -D _DEBUG -O0 -levent
CRELEASEFLAGS:= -Wall -Wextra --pedantic -std=gnu99 -c -D NDEBUG -O2
LDRELEASEFLAGS:= -Wall -Wextra --pedantic -std=gnu99 -D NDEBUG -O2 -levent
SOURCES      := $(wildcard *.c)

ifeq ($(debuglevel), 1)
	LDFLAGS = $(LDDEBUGFLAGS)
	CFLAGS  = $(CDEBUGFLAGS)
else
	LDFLAGS = $(LDRELEASEFLAGS)
	CFLAGS  = $(CRELEASEFLAGS)
endif

# pliki zrodlowe
MAINOBJECTS := $(subst .c,.o,$(shell grep -l main $(SOURCES)))
# pliki obiektowe, ktore zawieraja definicje funkcji main
ALL         := $(subst .o,,$(MAINOBJECTS))
# pliki wykonywalne (powstaja ze zrodel zawierajacych definicje main)
DEPENDS     := $(subst .c,.d,$(SOURCES))
# makefefiles dla kazdego z plikow zrodlowych
ALLOBJECTS  := $(subst .c,.o,$(SOURCES))
# wszystkie pliki obiektowe
OBJECTS     := $(filter-out $(MAINOBJECTS),$(ALLOBJECTS))
# pliki obiektowe, ktore nie zawieraja definicji main

all: $(DEPENDS) $(ALL)

$(DEPENDS) : %.d : %.c                          # tworzenie submakefiles
	$(CC) -MM $< > $@                       #       - zaleznosc
	@echo -e: "\t"$(CC) $(CFLAGS) $< >> $@  #       - polecenie kompilacji

$(ALL) : % : %.o $(OBJECTS)                     # konsolidacja
	$(CC) $(LDFLAGS) -o $@ $^

-include $(DEPENDS)                             # dolaczenie submakefiles

clean:
	-rm -f *.o $(ALL) $(ALLOBJECTS) $(DEPENDS) *~

remake: clean all
