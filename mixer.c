/* Jan Rydzewski 332461 */

#include "mixer.h"
#include "debug.h"
#include <stdint.h>

void mixer(
	struct mixer_input* inputs, size_t n,  // tablica struktur mixer_input, po jednej strukturze na każdą
	                                       // kolejkę FIFO w stanie ACTIVE
	void* output_buf,                      // bufor, w którym mikser powinien umieścić dane do wysłania
	size_t* output_size,                   // początkowo rozmiar output_buf, następnie mikser umieszcza
	                                       // w tej zmiennej liczbę bajtów zapisanych w output_buf
	unsigned long tx_interval_ms           // wartość zmiennej TX_INTERVAL
){
	/* wstepne kalkulacje */
	int16_t* bufor = (int16_t*) output_buf;
	size_t rozmiar_bufora = *output_size / sizeof(int16_t);
	size_t chciany_rozmiar_wyjscia = 176 * tx_interval_ms / sizeof(int16_t);
	
	/* sprawdzenie rozmiaru bufora */
	size_t rozmiar_wyjscia = chciany_rozmiar_wyjscia;
	if(rozmiar_bufora < chciany_rozmiar_wyjscia){
		warn("mixer: bufor za maly, robie co moge z tym co dostalem");
		rozmiar_wyjscia = rozmiar_bufora;
	}
	*output_size = rozmiar_wyjscia * sizeof(int16_t);
	
	/* zerowanie bufora */
	for(size_t i = 0; i < rozmiar_wyjscia; i++){
		bufor[i] = (int16_t)0;
	}
	
	/* dodawanie liczb od klientów */
	for(size_t i = 0; i < n; i++){
		
		/* kalkulacje */
		int16_t* wejscie = (int16_t*)inputs[i].data;
		size_t rozmiar_wejscia = inputs[i].len / sizeof(int16_t);
		if(rozmiar_wejscia > rozmiar_wyjscia){
			rozmiar_wejscia = rozmiar_wyjscia;
		}
		
		/* dodawanie */
		for(
			size_t ii = 0;
			ii < rozmiar_wejscia;
			ii++
		){
			int16_t nowy = bufor[ii] + wejscie[ii];
			if(wejscie[ii] > 0 && nowy < bufor[ii]){
				/* overflow */
				bufor[ii] = INT16_MAX;
			}else if(wejscie[ii] < 0 && nowy > bufor[ii]){
				/* underflow */
				bufor[ii] = INT16_MIN;
			}else{
				/* ok */
				bufor[ii] = nowy;
			}
		}
		
		/* consumed */
		inputs[i].consumed = rozmiar_wejscia * sizeof(int16_t);
	}
}
