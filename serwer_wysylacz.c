/* Jan Rydzewski 332461 */

#include "serwer_wysylacz.h"
#include "datagram.h"
#include <stdlib.h>
#include "debug.h"


struct SerwerWysylacz_{
	Serwer* serwer;
	
	unsigned int numer_nastepnego;
	Segment** wyslane;
	unsigned int dlugosc_historii;
};

SerwerWysylacz* serwer_wysylacz_nowy(Serwer* s, unsigned int n){
	
	spam("wysylacz: robienie");
	
	SerwerWysylacz* w = calloc(1, sizeof(SerwerWysylacz));
	if(w == NULL){
		erro("wysylacz: nie udal sie alloc");
		return NULL;
	}
	
	w->wyslane = calloc(n, sizeof(Segment*));
	if(w->wyslane == NULL){
		erro("wysylacz: nie udal sie alloc wyslanych");
		serwer_wysylacz_usun(w);
		return NULL;
	}
	
	w->dlugosc_historii = n;
	w->numer_nastepnego = 0;
	
	w->serwer = s;
	
	spam("wysylacz: zrobiony");
	
	return w;
}

void serwer_wysylacz_usun(SerwerWysylacz* w){
	spam("wysylacz: usuwanie");
	
	if(w != NULL){
		for(unsigned int i = 0; i < w->dlugosc_historii; i++){
			segment_usun(w->wyslane[i]);
		}
		free(w->wyslane);
		free(w);
	}
	
	spam("wysylacz: usuniety");
}

bool serwer_wysylacz_wyslij(SerwerWysylacz* w, Segment* s){
	/* zapis w historii */
	segment_usun(w->wyslane[w->numer_nastepnego % w->dlugosc_historii]);
	w->wyslane[w->numer_nastepnego % w->dlugosc_historii] = segment_kopiuj(s);
	
	if(w->wyslane[w->numer_nastepnego % w->dlugosc_historii] == NULL){
		warn("wysylacz: NULLowy segment w kolejce wyslanych");
	}
	
	/* wysylanie do klientów */
	for(
		Element* e = lista_daj_poczatek(serwer_daj_liste_klientow(w->serwer));
		lista_czy_w_liscie(e);
		e = lista_daj_nastepny(e)
	){
		SerwerKlient* k = (SerwerKlient*)lista_dane_elementu(e);
		if(!serwer_klient_wyslij_dane(
			k,
			w->wyslane[w->numer_nastepnego % w->dlugosc_historii],
			w->numer_nastepnego
		)){
			spam("wysylacz: nie udalo sie wyslac danych do klienta");
		}
	}
	
	/* nastepny */
	w->numer_nastepnego++;
	
	return true;
}

bool serwer_wysylacz_retransmituj(SerwerWysylacz* w, SerwerKlient* k, unsigned int n){
	if(n >= w->numer_nastepnego){
		/* reqest from the future */
		info("wysylacz: żądanie retransmisji z przyszłości (DATA %u)", n);
		return false;
	}
	
	if(n + w->dlugosc_historii < w->numer_nastepnego){
		/* to było dawno temu */
		info("wysylacz: żądanie dawnych kawałków (DATA %u)", n);
		n = w->numer_nastepnego - w->dlugosc_historii;
	}
	
	while(n < w->numer_nastepnego){
		spam("wysylacz: retransmisja (DATA %u)", n);
		
		if(!serwer_klient_wyslij_dane(
			k,
			w->wyslane[n % w->dlugosc_historii],
			n
		)){
			info("wysylacz: nie udalo sie retransmitowac");
			return false;
		}
		
		n++;
	}
	
	return true;
}
