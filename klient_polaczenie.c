/* Jan Rydzewski 332461 */

#include "klient_polaczenie.h"
#include <stdio.h>
#include <stdlib.h>
#include "czytaj_linie.h"
#include <unistd.h>
#include <string.h>
#include "debug.h"
#include "protokol.h"

/* head */

struct KlientPolaczenie_{
	Klient* klient;
	evutil_socket_t socket_UDP;
	evutil_socket_t socket_TCP;
	int id;
	struct event* event_sa_dane_UDP;
	struct event* event_sa_dane_TCP;
	struct event* keepalive_timer;
	struct event* event_serwer_milczy;
};

bool przywitaj_sie(KlientPolaczenie*);
void wyslij_keepalive(KlientPolaczenie*);
void callback_wyslij_keepalive(evutil_socket_t, short, void*);
bool klient_polaczenie_wyslij_po_UDP(KlientPolaczenie*, void*, size_t); /*?*/
void callback_sa_dane_TCP(evutil_socket_t, short, void*);
void sa_dane_TCP(KlientPolaczenie*);
void callback_sa_dane_UDP(evutil_socket_t, short, void*);
void sa_dane_UDP(KlientPolaczenie*);
void callback_serwer_milczy(evutil_socket_t, short, void*);
bool serwer_sie_odezwal(KlientPolaczenie*);


/* publiczne */

KlientPolaczenie* klient_polaczenie_nowe(Klient* klient, char* host, char* port){
	spam("polaczenie: nowe");
	
	/* malloc */
	KlientPolaczenie* polaczenie = calloc(1, sizeof(KlientPolaczenie));
	if(polaczenie == NULL){
		erro("polaczenie: alloc walnął");
		return NULL;
	}
	polaczenie->socket_TCP = -1;
	polaczenie->socket_UDP = -1;
	
	/* ojciec */
	polaczenie->klient = klient;
	
	/* parsowanie adresu */
	struct addrinfo addrinfo_hints;
	memset(&addrinfo_hints, 0, sizeof(addrinfo_hints));
	addrinfo_hints.ai_family = AF_INET;
	addrinfo_hints.ai_socktype = SOCK_STREAM;
	addrinfo_hints.ai_protocol = IPPROTO_TCP;
	
	struct addrinfo *addrinfo_result;
	int err = getaddrinfo(host, port, &addrinfo_hints, &addrinfo_result);
	if (err != 0){
		erro("polaczenie: getaddrinfo: %s", gai_strerror(err));
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	/* TCP */
	polaczenie->socket_TCP = socket(addrinfo_result->ai_family, SOCK_STREAM, addrinfo_result->ai_protocol);
	if (polaczenie->socket_TCP == -1){
		erro("polaczenie: socket TCP");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	if (connect(
		polaczenie->socket_TCP,
		addrinfo_result->ai_addr,
		addrinfo_result->ai_addrlen
	) < 0){
		erro("polaczenie: connect TCP");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	/* UDP */
	polaczenie->socket_UDP = socket(addrinfo_result->ai_family, SOCK_DGRAM, 0);
	if (polaczenie->socket_UDP == -1){
		erro("polaczenie: socket UDP");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	if (connect(
		polaczenie->socket_UDP,
		addrinfo_result->ai_addr,
		addrinfo_result->ai_addrlen
	) < 0){
		erro("polaczenie: connect UDP");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	/* free addrinfo */
	freeaddrinfo(addrinfo_result);
	
	/* powitanie */
	if(!przywitaj_sie(polaczenie)){
		erro("polaczenie: nie dalo sie przywitac");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	/* keepalive */
	polaczenie->keepalive_timer = event_new(
		klient_daj_event_base(klient),
		-1,
		EV_PERSIST,
		callback_wyslij_keepalive,
		(void*)polaczenie
	);
	struct timeval interval = { 0, (1000*1000)/CZESTOTLIWOSC_KEEPALIVE };
	if(
		polaczenie->keepalive_timer == NULL ||
		event_add(polaczenie->keepalive_timer, &interval) == -1
	){
		erro("polaczenie: nie dalo sie zrobic timera");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	/* event odbieranie TCP */
	evutil_make_socket_nonblocking(polaczenie->socket_TCP);
	polaczenie->event_sa_dane_TCP = event_new(
		klient_daj_event_base(klient),
		polaczenie->socket_TCP,
		EV_READ | EV_PERSIST,
		callback_sa_dane_TCP,
		(void*)polaczenie
	);
	if(
		polaczenie->event_sa_dane_TCP == NULL ||
		event_add(polaczenie->event_sa_dane_TCP, NULL) == -1
	){
		erro("polaczenie: nie dalo sie zrobic eventu odbierajacego TCP");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	/* event odbieranie UDP */
	evutil_make_socket_nonblocking(polaczenie->socket_UDP);
	polaczenie->event_sa_dane_UDP = event_new(
		klient_daj_event_base(klient),
		polaczenie->socket_UDP,
		EV_READ | EV_PERSIST,
		callback_sa_dane_UDP,
		(void*)polaczenie
	);
	if(
		polaczenie->event_sa_dane_UDP == NULL ||
		event_add(polaczenie->event_sa_dane_UDP, NULL) == -1
	){
		erro("polaczenie: nie dalo sie zrobic eventu odbierajacego UDP");
		klient_polaczenie_usun(polaczenie);
		return NULL;
	}
	
	/* event na nieaktywnosc */
	polaczenie->event_serwer_milczy = event_new(
		klient_daj_event_base(klient),
		-1,
		EV_TIMEOUT,
		callback_serwer_milczy,
		(void*)polaczenie
	);
	if(
		polaczenie->event_serwer_milczy == NULL ||
		!serwer_sie_odezwal(polaczenie)
	){
		erro("polaczenie: nie udalo sie zrobi timeouta na nieaktywnosc");
		klient_polaczenie_usun(polaczenie);
		return false;
	}
	
	spam("polaczenie: zrobione");
	
	return polaczenie;
	
}

void klient_polaczenie_usun(KlientPolaczenie* polaczenie){
	if(polaczenie == NULL){
		return;
	}
	
	/* wywalenie eventów */
	if(polaczenie->keepalive_timer != NULL){
		event_free(polaczenie->keepalive_timer);
	}
	if(polaczenie->event_sa_dane_TCP != NULL){
		event_free(polaczenie->event_sa_dane_TCP);
	}
	if(polaczenie->event_sa_dane_UDP != NULL){
		event_free(polaczenie->event_sa_dane_UDP);
	}
	if(polaczenie->event_serwer_milczy != NULL){
		event_free(polaczenie->event_serwer_milczy);
	}
	
	/* TCP */
	close(polaczenie->socket_TCP);
	
	/* UDP */
	close(polaczenie->socket_UDP);
	
	/* free */
	free(polaczenie);
}

bool klient_polaczenie_wyslij_datagram(KlientPolaczenie* p, Datagram* d){
	return datagram_wyslij(d, p->socket_UDP);
}

bool przywitaj_sie(KlientPolaczenie* polaczenie){
	spam("polaczenie: odczytujemy powitanie z TCP");
	
	char* linia = czytaj_linie(polaczenie->socket_TCP);
	if(linia == NULL){
		erro("polaczenie: sie nie udalo odczytac powitania");
		free(linia);
		return false;
	}
	
	if(sscanf(linia, "CLIENT %d\n", &(polaczenie->id)) != 1){
		erro("polaczenie: nieprawidlowy format powitania TCP <%s>", linia);
		free(linia);
		return false;
	}
	free(linia);
	
	spam("polacznie: odbrano numer klienta %d, odsylam po UDP", polaczenie->id);
	
	Datagram* d = protokol_zrob_CLIENT(polaczenie->id);
	if(!datagram_wyslij(d, polaczenie->socket_UDP)){
		erro("polaczenie: nie udalo sie wyslac powitania UDP");
		datagram_usun(d);
		return false;
	}
	datagram_usun(d);
	
	spam("polaczenie: powitanie wyslane po UDP");
	
	return true;
}

bool klient_polaczenie_wyslij_po_UDP(KlientPolaczenie* p, void* dane, size_t rozmiar){
	return send(p->socket_UDP, dane, rozmiar, 0) != -1;
}

/* prywatne */

void wyslij_keepalive(KlientPolaczenie* p){
	Datagram* d = protokol_zrob_KEEPALIVE();
	if(!klient_polaczenie_wyslij_datagram(p, d)){
		erro("polaczenie: nie udalo sie wyslac KEEPALIVE");
	}
	datagram_usun(d);
}

void sa_dane_TCP(KlientPolaczenie* p){
	char bufor[11];
	ssize_t n = read(p->socket_TCP, (void*)bufor, 10);
	if(n == -1){
		erro("polaczenie: nie udalo sie odczytac z tcp");
		klient_stop(p->klient);
		return;
	}
	bufor[n] = (char)0;
	fprintf(stderr, "%s", bufor);
}



void sa_dane_UDP(KlientPolaczenie* p){
	Datagram* d = datagram_odbierz(p->socket_UDP);
	if(d == NULL){
		warn("polaczennie: nie udalo sie odebrac datagramu");
		return;
	}
	
	bool odbieracz_chcial = klient_odbieracz_odbierz_dane(klient_daj_odbieracza(p->klient), d);
	bool wysylacz_chcial = klient_wysylacz_odbierz_dane(klient_daj_wysylacza(p->klient), d);
	
	serwer_sie_odezwal(p);
	
	if(
		!odbieracz_chcial && !wysylacz_chcial
	){
		warn("polaczenie: dziki naglowek datagramu: <%s>", datagram_pozycz_naglowek(d));
	}
	
	datagram_usun(d);
}


bool serwer_sie_odezwal(KlientPolaczenie* p){
	event_del(p->event_serwer_milczy);
	struct timeval interval = { 0, (1000*1000)*TIMEOUT_ROZLACZENIA };
	if(
		event_add(p->event_serwer_milczy, &interval) == -1
	){
		warn("polaczenie: nie dalo sie zresetowac timeouta");
		return false;
	}
	return true;
}

/* callbacki */

void callback_sa_dane_TCP(
	evutil_socket_t sock,
	short what,
	void* arg
){
	(void)sock;
	(void)what;
	sa_dane_TCP((KlientPolaczenie*)arg);
}

void callback_sa_dane_UDP(
	evutil_socket_t sock,
	short what,
	void* arg
){
	(void)sock;
	(void)what;
	sa_dane_UDP((KlientPolaczenie*)arg);
}

void callback_wyslij_keepalive(
	evutil_socket_t sock,
	short what,
	void* arg
){
	(void)sock;
	(void)what;
	wyslij_keepalive((KlientPolaczenie*)arg);
}

void callback_serwer_milczy(
	evutil_socket_t sock,
	short what,
	void* arg
){
	(void)sock;
	(void)what;
	info("polaczenie: serwer milczy, konczymy");
	klient_stop(((KlientPolaczenie*)arg)->klient);
}
