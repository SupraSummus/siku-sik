/* Jan Rydzewski 332461 */

#ifndef SERWER_H
#define SERWER_H

typedef struct Serwer_ Serwer;
typedef struct SerwerLaczyciel_ SerwerLaczyciel;
typedef struct SerwerKlient_ SerwerKlient;
typedef struct SerwerUDP_ SerwerUDP;
typedef struct SerwerMikser_ SerwerMikser;
typedef struct SerwerWysylacz_ SerwerWysylacz;

#include "ustawienia.h"
#include "serwer_rdzen.h"
#include "serwer_laczyciel.h"
#include "serwer_klient.h"
#include "serwer_udp.h"
#include "serwer_mikser.h"
#include "serwer_wysylacz.h"

#endif
