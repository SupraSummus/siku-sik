/* Jan Rydzewski 332461 */

#ifndef SEGMENT_H
#define SEGMENT_H

#include <unistd.h>
#include <stdbool.h>
#include <netinet/in.h>

/**
 * Segment - taki tam kawałek pamięci...
 */

typedef struct Segment_ Segment;

Segment* segment_nowy(void*, size_t);
Segment* segment_kopiuj(Segment*);

bool segment_dopisz(Segment*, Segment*);
Segment* segment_utnij_poczatek(Segment*, size_t);

void segment_usun(Segment*);

bool segment_wypelnij(Segment*, void*, size_t);

/* getery */
void* segment_pozycz_dane(Segment*);
size_t segment_daj_rozmiar(Segment*);

/* wypisywanie */
bool segment_wypisz_sie(Segment*, int);
bool segment_wyslij(Segment*, int);
bool segment_wyslij_do_ipv6(Segment*, int, struct sockaddr_in6*);

#endif
