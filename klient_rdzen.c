/* Jan Rydzewski 332461 */

#include "klient.h"
#include <stdio.h>
#include <stdlib.h>
#include "debug.h"
#include <signal.h>

struct Klient_{
	struct event_base* event_base;
	
	KlientPolaczenie* polaczenie;
	KlientWysylacz* wysylacz;
	KlientOdbieracz* odbieracz;
	
	struct event* event_sigint;
};

static void callback_sigint(evutil_socket_t, short, void*);

void klient_nowy(char* host, char* port, int limit_retransmisji){
	spam("rdzen: robienie");
	
	/* alloc */
	Klient* klient = calloc(1, sizeof(Klient));
	if(klient == NULL){
		erro("rdzeń: alloc nie wyszedł");
		return;
	}
	
	if(
		(klient->event_base = event_base_new()) == NULL ||
		(klient->polaczenie = klient_polaczenie_nowe(klient, host, port)) == NULL ||
		(klient->wysylacz = klient_wysylacz_nowy(klient, STDIN_FILENO)) == NULL ||
		(klient->odbieracz = klient_odbieracz_nowy(klient, STDOUT_FILENO, limit_retransmisji)) == NULL
	){
		erro("rdzen: nie udało sie zrobic elementow");
		klient_usun(klient);
		return;
	}
	
	/* handler siginta */
	klient->event_sigint = evsignal_new(klient->event_base, SIGINT, callback_sigint, (void*)klient);
	if(
		klient->event_sigint == NULL ||
		event_add(klient->event_sigint, NULL) < 0
	){
		erro("klient: nie dalo sie zrobic wychodzenia na SIGINT");
		klient_usun(klient);
		return;
	}
	
	spam("rdzen: zrobiony, odpalam");
	
	event_base_dispatch(klient->event_base);
	
	spam("rdzen: koniec petli, usuwam");
	
	klient_usun(klient);
}

void klient_usun(Klient* klient){
	spam("rdzen: usuwanie");
	
	if(klient != NULL){
		klient_wysylacz_usun(klient->wysylacz);
		klient_polaczenie_usun(klient->polaczenie);
		klient_odbieracz_usun(klient->odbieracz);
		
		if(klient->event_sigint != NULL){
			event_free(klient->event_sigint);
		}
		if(klient->event_base != NULL){
			event_base_free(klient->event_base);
		}
		free(klient);
	}
	
	spam("rdzen: usuniety");
}

void klient_stop(Klient* k){
	event_base_loopexit(k->event_base, NULL);
}

struct event_base* klient_daj_event_base(Klient* k){
	return k->event_base;
}

KlientWysylacz* klient_daj_wysylacza(Klient* k){
	return k->wysylacz;
}

KlientPolaczenie* klient_daj_polaczenie(Klient* k){
	return k->polaczenie;
}

KlientOdbieracz* klient_daj_odbieracza(Klient* k){
	return k->odbieracz;
}

void callback_sigint(evutil_socket_t fd, short what, void* arg){
	(void)fd;
	(void)what;
	klient_stop(arg);
}
