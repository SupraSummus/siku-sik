/* Jan Rydzewski 332461 */

#ifndef KLIENT_RDZEN_H
#define KLIENT_RDZEN_H

#include <stdbool.h> /* bool */
#include <event2/event.h> /* libevent */
#include "klient.h"

void klient_nowy(char*, char*, int);
void klient_stop(Klient*);

void klient_usun(Klient*);

/* elementy */

struct event_base* klient_daj_event_base(Klient*);
KlientWysylacz* klient_daj_wysylacza(Klient*);
KlientPolaczenie* klient_daj_polaczenie(Klient*);
KlientOdbieracz* klient_daj_odbieracza(Klient*);

#endif
