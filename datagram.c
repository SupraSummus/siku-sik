/* Jan Rydzewski 332461 */

#include "datagram.h"
#include <string.h>
#include "segment.h"
#include <stdio.h>
#include "ustawienia.h"
#include <stdarg.h>
#include <sys/socket.h>
#include <stdlib.h>
#include "debug.h"

struct Datagram_{
	char naglowek[MAKSYMALNY_ROZMIAR_NAGLOWKA];
	Segment* dane;
};

Datagram* datagram_nowy(const char* naglowek, void* dane, size_t rozmiar){
	Datagram* d = calloc(1, sizeof(Datagram));
	
	strcpy(d->naglowek, naglowek);
	
	d->dane = segment_nowy(dane, rozmiar);
	
	return d;
}

Datagram* datagram_nowy_printf(void* dane, size_t rozmiar, const char* format, ...){
	va_list l;
	
	char naglowek[MAKSYMALNY_ROZMIAR_NAGLOWKA];
	
	va_start(l, format);
	vsprintf(naglowek, format, l);
	va_end(l);
	
	return datagram_nowy(naglowek, dane, rozmiar);
}

Datagram* datagram_nowy_segment_printf(Segment* s, const char* format, ...){
	va_list l;
	
	char naglowek[MAKSYMALNY_ROZMIAR_NAGLOWKA];
	
	va_start(l, format);
	vsprintf(naglowek, format, l);
	va_end(l);
	
	return datagram_nowy(naglowek, segment_pozycz_dane(s), segment_daj_rozmiar(s));
}

Datagram* datagram_odbierz(int fd){
	char bufor[MAKSYMALNY_ROZMIAR_ODBIERANEGO_DATAGRAMU];
	char naglowek[MAKSYMALNY_ROZMIAR_NAGLOWKA];
	
	ssize_t n = recv(fd, bufor, MAKSYMALNY_ROZMIAR_ODBIERANEGO_DATAGRAMU, 0);
	if(n == 0 || n == -1){
		/* nie udalo sie odczytac */
		return NULL;
	}
	
	void* nastepny = memccpy(naglowek, bufor, (int)'\n', MAKSYMALNY_ROZMIAR_NAGLOWKA);
	if(nastepny == NULL){
		/* nie znaleziono \n */
		return NULL;
	}
	if(*((char*)nastepny - 1) != '\n'){
		/* prawdopodobnie bufor za maly */
		return NULL;
	}
	
	/* \n -> \0 */
	*((char*)nastepny - 1) = '\0';
	
	size_t dlugosc_naglowka = strlen(naglowek) + 1;
	
	return datagram_nowy(
		naglowek,
		(void*)((char*)bufor + dlugosc_naglowka),
		n - dlugosc_naglowka
	);
}

Datagram* datagram_odbierz_od_ipv6(int fd, struct sockaddr_in6* addr){
	char bufor[MAKSYMALNY_ROZMIAR_ODBIERANEGO_DATAGRAMU];
	socklen_t dlugosc_adresu = sizeof(*addr);
	ssize_t n = recvfrom(
		fd,
		bufor,
		MAKSYMALNY_ROZMIAR_ODBIERANEGO_DATAGRAMU,
		0,
		(struct sockaddr*)addr,
		&dlugosc_adresu
	);
	if(n == 0 || n == -1 || dlugosc_adresu != sizeof(*addr)){
		/* nie udalo sie odczytac albo to nie bylo ipv6 */
		return NULL;
	}
	
	char naglowek[MAKSYMALNY_ROZMIAR_NAGLOWKA];
	void* nastepny = memccpy(naglowek, bufor, (int)'\n', MAKSYMALNY_ROZMIAR_NAGLOWKA);
	if(nastepny == NULL){
		/* nie znaleziono \n */
		return NULL;
	}
	if(*((char*)nastepny - 1) != '\n'){
		/* prawdopodobnie bufor za maly */
		return NULL;
	}
	
	/* \n -> \0 */
	*((char*)nastepny - 1) = '\0';
	
	size_t dlugosc_naglowka = strlen(naglowek) + 1;
	
	return datagram_nowy(
		naglowek,
		(void*)((char*)bufor + dlugosc_naglowka),
		n - dlugosc_naglowka
	);
}

bool datagram_wyslij(Datagram* d, int fd){
	if(d == NULL || fd < 0){
		warn("datagram_wyslij(%p, %i)", (void*)d, fd);
		return false;
	}
	
	Segment* s = datagram_jako_segment(d);
	bool ok = segment_wyslij(s, fd);
	segment_usun(s);
	return ok;
}

bool datagram_wyslij_do_ipv6(Datagram* d, int fd, struct sockaddr_in6* addr){
	if(d == NULL || fd < 0 || addr == NULL){
		warn("datagram_wyslij_po_ipv6(%p, %i, %p)", (void*)d, fd, (void*)addr);
		return false;
	}
	Segment* s = datagram_jako_segment(d);
	bool ok = segment_wyslij_do_ipv6(s, fd, addr);
	segment_usun(s);
	return ok;
}

int datagram_scanf(Datagram* d, const char* format, ...){
	va_list l;
	
	va_start(l, format);
	int r = vsscanf(d->naglowek, format, l);
	va_end(l);
	
	return r;
}

void datagram_usun(Datagram* d){
	if(d != NULL){
		segment_usun(d->dane);
	}
	free(d);
}

char* datagram_pozycz_naglowek(Datagram* d){
	return d->naglowek;
}

Segment* datagram_daj_segment(Datagram* d){
	return d->dane;
}

Segment* datagram_jako_segment(Datagram* d){
	if(d == NULL){
		return false;
	}
	
	void* bufor = malloc(strlen(d->naglowek) + 1 + segment_daj_rozmiar(d->dane));
	size_t uzyte = 0;
	
	strcpy(bufor, d->naglowek);
	uzyte += strlen(d->naglowek);
	 
	((char*)bufor)[uzyte] = '\n';
	uzyte++;
	
	memcpy(
		(void*)((char*)bufor + uzyte),
		segment_pozycz_dane(d->dane),
		segment_daj_rozmiar(d->dane)
	);
	uzyte += segment_daj_rozmiar(d->dane);
	
	Segment* s = segment_nowy(bufor, uzyte);
	free(bufor);
	
	return s;
}
