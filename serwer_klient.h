/* Jan Rydzewski 332461 */

#ifndef SERWER_KLIENT_H
#define SERWER_KLIENT_H

#include <stdbool.h> /* bool */
#include "serwer.h"
#include "datagram.h"

/**
 * obiekt trzymający informacje o kliencie podłącznym do serwera
 */

SerwerKlient* serwer_klient_nowy(Serwer*, evutil_socket_t, unsigned, size_t, size_t, size_t);
void serwer_klient_usun(SerwerKlient*);

char* serwer_klient_daj_raport(SerwerKlient*);
bool serwer_klient_wyslij_raport(SerwerKlient*, const char*);

bool serwer_klient_wyslij_dane(SerwerKlient*, Segment*, unsigned int);
bool serwer_klient_odbierz_dane(SerwerKlient*, Datagram*, struct sockaddr_in6*);

Segment* serwer_klient_daj_dane(SerwerKlient*, size_t);
bool serwer_klient_usun_dane(SerwerKlient*, size_t);

void serwer_klient_rozlacz_delikatnie(SerwerKlient*);

#endif
