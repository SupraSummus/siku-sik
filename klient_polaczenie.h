/* Jan Rydzewski 332461 */

#ifndef KLIENT_POLACZENIE_H
#define KLIENT_POLACZENIE_H

#include "klient.h"
#include <event2/event.h> /* libevent */
#include "datagram.h"

/**
 * klient polaczenie - obiekt utrzymujący polaczenie UDP i TCP
 */

KlientPolaczenie* klient_polaczenie_nowe(Klient*, char*, char*);
void klient_polaczenie_usun(KlientPolaczenie*);

bool klient_polaczenie_wyslij_datagram(KlientPolaczenie*, Datagram*);

#endif
