/* Jan Rydzewski 332461 */

#include "czytaj_linie.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>

char* czytaj_linie(int fd){
	size_t dlugosc_bufora = 10;
	size_t wczytane_znaki = 0;
	char* bufor = malloc(dlugosc_bufora);
	bool blad = false;
	
	while(true){
		ssize_t n = read(fd, bufor + wczytane_znaki, 1);
		if(n == -1){
			blad = true;
			break;
		}else if(n == 0){
			break;
		}else{
			wczytane_znaki += n;
			if(bufor[wczytane_znaki - n] == '\n'){
				break;
			}else{
				if(wczytane_znaki > (dlugosc_bufora - 3)){
					dlugosc_bufora *= 2;
					bufor = realloc(bufor, dlugosc_bufora);
				}
			}
		}
	}
	
	if(blad){
		free(bufor);
		return NULL;
	}
	
	bufor[wczytane_znaki] = (char)0;
	
	return bufor;
}
