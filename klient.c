/* Jan Rydzewski 332461 */

#include <stdlib.h>
#include "klient.h"
#include "debug.h"

int main(int argc, char** argv){
	char* p = "12461";
	char* s = "localhost";
	char* X = "10";
	char c;
	
	opterr = 0;
	
	while((c = getopt (argc, argv, "p:s:X:")) != -1){
		switch(c){
			case 'p':
				p = optarg;
				break;
			case 's':
				s = optarg;
				break;
			case 'X':
				X = optarg;
				break;
			default:
				break;
		}
	}
	
	info("odpalam sie z opcjami:");
	info("host:               %s", s);
	info("port:               %s", p);
	info("limit retransmisji: %i", atoi(X));
	
	while(true){
		spam("nowa instancja klienta");
		
		/* nowy */
		klient_nowy(s, p, atoi(X));
		
		/* poczekac */
		usleep(500*1000);
	}
}
