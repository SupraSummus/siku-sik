/* Jan Rydzewski 332461 */

#ifndef SERWER_WYSYLACZ_H
#define SERWER_WYSYLACZ_H

#include <stdbool.h>
#include "segment.h"
#include "serwer.h"

/**
 * SerwerWysylacz rozsyla dane do klientów.
 * Można go poprosić o retransmisję, jeśli ktoś zapragnie.
 */

SerwerWysylacz* serwer_wysylacz_nowy(Serwer*, unsigned int);
void serwer_wysylacz_usun(SerwerWysylacz*);

bool serwer_wysylacz_wyslij(SerwerWysylacz*, Segment*);
bool serwer_wysylacz_retransmituj(SerwerWysylacz*, SerwerKlient*, unsigned int);


#endif
