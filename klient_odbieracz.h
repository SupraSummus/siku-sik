/* Jan Rydzewski 332461 */

#ifndef KLIENT_ODBIERACZ_H
#define KLIENT_ODBIERACZ_H

#include "klient.h"
#include "datagram.h"
#include <stdbool.h>

/**
 * KlientOdbieracz zajmuje się kompletowaniem odebranych danych.
 * Z tego powodu trzeba mu wywoływać klient_odbieracz_odbierz_dane() gdy przyjdą dane.
 */

KlientOdbieracz* klient_odbieracz_nowy(Klient*, int, int);
void klient_odbieracz_usun(KlientOdbieracz*);

bool klient_odbieracz_odbierz_dane(KlientOdbieracz*, Datagram*);

#endif
