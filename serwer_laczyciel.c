/* Jan Rydzewski 332461 */

#include "serwer.h"
#include <stdbool.h> /* bool */
#include <event2/event.h> /* libevent */
#include <stdio.h> /* printf */
#include <stdlib.h> /* malloc */
#include <unistd.h> /* close */
#include "debug.h"
#include <string.h>

struct SerwerLaczyciel_{
	Serwer* serwer;
	struct event* event_nowe_polaczenie;
	evutil_socket_t sluchajacy_socket;
};

void serwer_laczyciel_przychodzace_polaczenie(evutil_socket_t, short, void*);

SerwerLaczyciel* serwer_laczyciel_nowy(
	Serwer* serwer,
	unsigned short int port
){
	spam("łączyciel: robienie");
	
	/* alloc */
	SerwerLaczyciel* laczyciel = calloc(1, sizeof(SerwerLaczyciel));
	if(laczyciel == NULL){
		erro("łączyciel: nie udalo sie zaalokowac");
		return NULL;
	}
	laczyciel->sluchajacy_socket = -1;
	
	/* ojciec */
	laczyciel->serwer = serwer;
	
	/* socket */
	if(
		(laczyciel->sluchajacy_socket = socket(AF_INET6, SOCK_STREAM, 0)) == -1 ||
		evutil_make_socket_nonblocking(laczyciel->sluchajacy_socket) == -1 ||
		evutil_make_listen_socket_reuseable(laczyciel->sluchajacy_socket) == -1
	){
		erro("łączyciel: walnęło robienie słuchającego socketu");
		serwer_laczyciel_usun(laczyciel);
		return NULL;
	}
	
	/* konfiguracja socketu TCP */
	struct sockaddr_in6 adres;
	memset(&adres, 0, sizeof(adres));
	adres.sin6_family = AF_INET6;
	adres.sin6_port = htons(port);
	adres.sin6_addr = in6addr_any;
	
	if(
		bind(
			laczyciel->sluchajacy_socket,
			(struct sockaddr *) &adres,
			sizeof(adres)
		) < 0
	){
		erro("łączyciel: nie da sie zbindować słuchajacego socketu");
		serwer_laczyciel_usun(laczyciel);
		return NULL;
	}
	
	/* rejestracja obsługi */
	laczyciel->event_nowe_polaczenie = event_new(
		serwer_daj_event_base(serwer),
		laczyciel->sluchajacy_socket,
		EV_READ|EV_PERSIST,
		serwer_laczyciel_przychodzace_polaczenie,
		(void*)laczyciel
	);
	if(
		laczyciel->event_nowe_polaczenie == NULL ||
		event_add(laczyciel->event_nowe_polaczenie, NULL) == -1
	){
		erro("łączyciel: nie mozna zrobic eventa");
		serwer_laczyciel_usun(laczyciel);
		return NULL;
	}
	
	/* słuchajże */
	if(listen(laczyciel->sluchajacy_socket, MAKSYMALNA_DLUGOSC_KOLEJKI_LISTEN) == -1){
		erro("łączyciel: listen");
		serwer_laczyciel_usun(laczyciel);
		return NULL;
	}
	
	spam("łączyciel: zrobiony");
	
	return laczyciel;
}

void serwer_laczyciel_usun(SerwerLaczyciel* laczyciel){
	spam("łączyciel: usuwanie");
	
	if(laczyciel != NULL){
		if(laczyciel->event_nowe_polaczenie != NULL){
			event_free(laczyciel->event_nowe_polaczenie);
		}
		close(laczyciel->sluchajacy_socket);
		free(laczyciel);
	}
	
	spam("łączyciel: usuniety");
}

void serwer_laczyciel_przychodzace_polaczenie(
	evutil_socket_t fd,
	short what,
	void* arg
){
	(void)fd;
	(void)what;
	SerwerLaczyciel* laczyciel = (SerwerLaczyciel*)arg;
	
	spam("łączyciel: nowe_polaczenie");
	
	evutil_socket_t polaczenie = accept(laczyciel->sluchajacy_socket, NULL, NULL);
	if(polaczenie == -1){
		erro("łączyciel: cos nie tak z acceptem, porzucam klienta");
	}else{
		if(serwer_nowy_klient(laczyciel->serwer, polaczenie) == NULL){
			warn("łączyciel: nie udalo sie stworzyc klienta");
		}
		close(polaczenie);
	}
}
