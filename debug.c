/* Jan Rydzewski 332461 */

#include "debug.h"
#include <stdio.h>
#include <stdarg.h>
#include "ustawienia.h"

const bool TODO = true;
const bool WARN = true;
const bool ERRO = true;
const bool INFO = true;
const bool SPAM = false;

void todo(const char* format, ...){
	if(TODO){
		va_list l;
		
		fprintf(stderr, "[TODO] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void warn(const char* format, ...){
	if(WARN){
		va_list l;
		
		fprintf(stderr, "[WARN] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void erro(const char* format, ...){
	if(ERRO){
		va_list l;
		
		fprintf(stderr, "[ERRO] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void info(const char* format, ...){
	if(INFO){
		va_list l;
		
		fprintf(stderr, "[INFO] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void spam(const char* format, ...){
	if(SPAM){
		va_list l;
		
		fprintf(stderr, "[SPAM] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}
