/* Jan Rydzewski 332461 */

#include "protokol.h"
#include <stdio.h>
#include "datagram.h"
#include "ustawienia.h"
#include <string.h>

/* CLIENT */
bool protokol_parsuj_CLIENT(Datagram* d, unsigned int* n){
	char c;
	return sscanf(datagram_pozycz_naglowek(d), "CLIENT %u%c", n, &c) == 1;
}

Datagram* protokol_zrob_CLIENT(unsigned int n){
	return datagram_nowy_printf(NULL, 0, "CLIENT %u", n);
}

/* UPLOAD */
bool protokol_parsuj_UPLOAD(Datagram* d, unsigned int* n){
	char c;
	return sscanf(datagram_pozycz_naglowek(d), "UPLOAD %u%c", n, &c) == 1;
}

Datagram* protokol_zrob_UPLOAD(unsigned int n, Segment* s){
	return datagram_nowy_segment_printf(s, "UPLOAD %u", n);
}

/* DATA */
bool protokol_parsuj_DATA(Datagram* d, unsigned int* nr, unsigned int* ack, unsigned int* win){
	char c;
	return sscanf(datagram_pozycz_naglowek(d), "DATA %u %u %u%c", nr, ack, win, &c) == 3;
}

Datagram* protokol_zrob_DATA(unsigned int nr, unsigned int ack, unsigned int win, Segment* s){
	return datagram_nowy_segment_printf(s, "DATA %u %u %u", nr, ack, win);
}

/* ACK */
bool protokol_parsuj_ACK(Datagram* d, unsigned int* ack, unsigned int* win){
	char c;
	return sscanf(datagram_pozycz_naglowek(d), "ACK %u %u%c", ack, win, &c) == 2;
}

Datagram* protokol_zrob_ACK(unsigned int ack, unsigned int win){
	return datagram_nowy_printf(NULL, 0, "ACK %u %u", ack, win);
}

/* RETRANSMIT */
bool protokol_parsuj_RETRANSMIT(Datagram* d, unsigned int* n){
	char c;
	return sscanf(datagram_pozycz_naglowek(d), "RETRANSMIT %u%c", n, &c) == 1;
}

Datagram* protokol_zrob_RETRANSMIT(unsigned int n){
	return datagram_nowy_printf(NULL, 0, "RETRANSMIT %u", n);
}

/* KEEPALIVE */
bool protokol_parsuj_KEEPALIVE(Datagram* d){
	char c;
	char s[MAKSYMALNY_ROZMIAR_NAGLOWKA];
	if(sscanf(datagram_pozycz_naglowek(d), "%s%c", s, &c) != 1){
		return false;
	}
	return strcmp("KEEPALIVE", s) == 0;
}

Datagram* protokol_zrob_KEEPALIVE(){
	return datagram_nowy_printf(NULL, 0, "KEEPALIVE");
}
