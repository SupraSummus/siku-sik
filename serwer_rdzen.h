/* Jan Rydzewski 332461 */

#ifndef SERWER_RDZEN
#define SERWER_RDZEN

#include <stdbool.h> /* bool */
#include <event2/event.h> /* libevent */
#include "lista.h"
#include "serwer.h"

void serwer_nowy(unsigned short int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int);
void serwer_usun(Serwer*);

//unsigned int serwer_daj_wolny_numer_klienta(Serwer*);
SerwerKlient* serwer_nowy_klient(Serwer*, int);
void serwer_usun_klienta(Serwer*, SerwerKlient*);

/* części, nieobiektowe getery */

struct event_base* serwer_daj_event_base(Serwer*);
Lista* serwer_daj_liste_klientow(Serwer*);
SerwerUDP* serwer_pozycz_UDP(Serwer*);
SerwerMikser* serwer_pozycz_mikser(Serwer*);
SerwerWysylacz* serwer_pozycz_wysylacza(Serwer*);

#endif
