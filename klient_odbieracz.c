/* Jan Rydzewski 332461 */

#include "klient_odbieracz.h"
#include "debug.h"
#include <stdlib.h>
#include "protokol.h"

struct KlientOdbieracz_{
	Klient* klient;
	int fd; /* tu wypisuje dane */
	unsigned int numer_oczekiwanego_segmentu;
	unsigned int najwiekszy_widziany;
	int limit_retransmisji;
};

KlientOdbieracz* klient_odbieracz_nowy(Klient* k, int fd, int limit_retransmisji){
	spam("odbieracz: robienie nowego");
	
	KlientOdbieracz* o = calloc(1, sizeof(KlientOdbieracz));
	if(o == NULL){
		return NULL;
	}
	
	o->klient = k;
	o->fd = dup(fd);
	o->numer_oczekiwanego_segmentu = 0;
	o->najwiekszy_widziany = 0;
	o->limit_retransmisji = limit_retransmisji;
	
	if(o->fd == -1){
		klient_odbieracz_usun(o);
		return NULL;
	}
	
	spam("odbieracz: zrobiony");
	
	return o;
}

void klient_odbieracz_usun(KlientOdbieracz* o){
	spam("odbieracz: usuwanie");
	
	if(o != NULL){
		if(o->fd != -1){
			close(o->fd);
		}
		free(o);
	}
	
	spam("odbieracz: usuniety");
}

bool klient_odbieracz_odbierz_dane(KlientOdbieracz* o, Datagram* d){
	
	unsigned int numer, ack, win;
	if(!protokol_parsuj_DATA(d, &numer, &ack, &win)){
		/* to nie jest poprawny pakiet DATA */
		return false;
	}
	
	if(numer > o->najwiekszy_widziany){
		o->najwiekszy_widziany = numer;
	}
	
	if(numer < o->numer_oczekiwanego_segmentu){
		
		spam("odbieracz: dostalismy pakiet z przeszlosci (%u, aktualnie oczekujemy na %u)", numer, o->numer_oczekiwanego_segmentu);
		
	}else if(numer == o->numer_oczekiwanego_segmentu){
		
		/* wszystko ok */
		o->numer_oczekiwanego_segmentu++;
		if(!segment_wypisz_sie(datagram_daj_segment(d), o->fd)){
			warn("odbieracz: nie udalo sie wypisac segmentu");
		}
		
	}else if(
		o->numer_oczekiwanego_segmentu < numer &&
		numer <= o->numer_oczekiwanego_segmentu + o->limit_retransmisji &&
		numer == o->najwiekszy_widziany
	){
		
		/* dziura, sprubujemy retransmisji */
		info("odbieracz: dziura, wysylam prosbe o retransmisje %u (bo dostalismy %u)", o->numer_oczekiwanego_segmentu, numer);
		Datagram* r = protokol_zrob_RETRANSMIT(o->numer_oczekiwanego_segmentu);
		if(!klient_polaczenie_wyslij_datagram(klient_daj_polaczenie(o->klient), r)){
			warn("odbieracz: nie udalo sie wyslac prosby o retransmisje");
		}
		datagram_usun(r);
		
	}else if(
		o->numer_oczekiwanego_segmentu + o->limit_retransmisji < numer
	){
		
		/* dziura ale za duza */
		info(
			"odbieracz: duza dziura, pomijam datagramy (%u - %u)",
			o->numer_oczekiwanego_segmentu,
			numer
		);
		o->numer_oczekiwanego_segmentu = numer + 1;
		if(!segment_wypisz_sie(datagram_daj_segment(d), o->fd)){
			warn("odbieracz: nie udalo sie wypisac segmentu");
		}
		
	}else{
		todo("klient_odbieracz_odbierz_dane(): else?");
	}
	
	return true;
}
