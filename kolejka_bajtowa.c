/* Jan Rydzewski 332461 */

#include "kolejka_bajtowa.h"
#include "lista.h"
#include <stdlib.h>
#include <string.h>
#include "debug.h"

struct KolejkaBajtowa_{
	Lista* segmenty;
	size_t rozmiar;
};

size_t zrob_segment_wiekszy_rowny(KolejkaBajtowa*, size_t);
size_t zrob_segment_rowny(KolejkaBajtowa*, size_t);

/* konstruktor */

KolejkaBajtowa* kolejka_bajtowa_nowa(){
	KolejkaBajtowa* k = calloc(1, sizeof(KolejkaBajtowa));
	if(k == NULL){
		return NULL;
	}
	
	k->segmenty = lista_nowa();
	if(k->segmenty == NULL){
		kolejka_bajtowa_usun(k);
		return NULL;
	}
	
	k->rozmiar = 0;
	
	return k;
}

void kolejka_bajtowa_usun(KolejkaBajtowa* k){
	if(k == NULL){
		return;
	}
	while(kolejka_bajtowa_daj_liczbe_segmentow(k) > 0){
		kolejka_bajtowa_usun_segment(k);
	}
	lista_usun(k->segmenty);
	free(k);
}

/* operacje segmentowe */

bool kolejka_bajtowa_wrzuc_segment(KolejkaBajtowa* k, Segment* s){
	if(k == NULL || s == NULL){
		warn("kolejka_bajtowa_wrzuc_segment: NULLowe argumenty");
		return false;
	}
	
	Segment* ss = segment_kopiuj(s);
	lista_dodaj_na_koncu(ss, k->segmenty);
	k->rozmiar += segment_daj_rozmiar(ss);
	return true;
}

Segment* kolejka_bajtowa_daj_segment(KolejkaBajtowa* k){
	if(k == NULL){
		warn("kolejka_bajtowa_daj_segment: kolejka NULL");
		return NULL;
	}
	if(kolejka_bajtowa_daj_liczbe_segmentow(k) == 0){
		warn("kolejka_bajtowa_daj_segment: kolejka pusta");
		return NULL;
	}
	
	Element* e = lista_daj_poczatek(k->segmenty);
	return segment_kopiuj(lista_dane_elementu(e));
}

Segment* kolejka_bajtowa_daj_i_usun_segment(KolejkaBajtowa* k){
	if(k == NULL){
		warn("kolejka_bajtowa_daj_i_usun_segment: kolejka NULL");
		return NULL;
	}
	if(kolejka_bajtowa_daj_liczbe_segmentow(k) == 0){
		warn("kolejka_bajtowa_daj_i_usun_segment: kolejka pusta");
		return NULL;
	}
	
	Element* e = lista_daj_poczatek(k->segmenty);
	Segment* s = lista_dane_elementu(e);
	k->rozmiar -= segment_daj_rozmiar(s);
	lista_usun_element(e);
	return s;
}

bool kolejka_bajtowa_usun_segment(KolejkaBajtowa* k){
	if(k == NULL){
		warn("kolejka_bajtowa_usun_segment: kolejka NULL");
		return NULL;
	}
	if(kolejka_bajtowa_daj_liczbe_segmentow(k) == 0){
		warn("kolejka_bajtowa_usun_segment: kolejka pusta");
		return NULL;
	}
	
	segment_usun(kolejka_bajtowa_daj_i_usun_segment(k));
	return true;
}

size_t kolejka_bajtowa_daj_liczbe_segmentow(KolejkaBajtowa* k){
	if(k == NULL){
		warn("kolejka_bajtowa_daj_liczbe_segmentow: kolejka NULL");
		return 0;
	}
	return lista_daj_liczbe_elementow(k->segmenty);
}

/* operacje bajtowe */

bool kolejka_bajtowa_wrzuc_dane(KolejkaBajtowa* k, Segment* s){
	if(k == NULL || s == NULL){
		warn("kolejka_bajtowa_wrzuc_dane: NULLowe argumenty");
		return false;
	}
	return kolejka_bajtowa_wrzuc_segment(k ,s);
}

Segment* kolejka_bajtowa_daj_dane(KolejkaBajtowa* k, size_t n){
	if(k == NULL){
		warn("kolejka_bajtowa_daj_dane: kolejka NULL");
		return NULL;
	}
	
	if(kolejka_bajtowa_daj_liczbe_segmentow(k) == 0){
		return segment_nowy(NULL, 0);
	}
	zrob_segment_rowny(k, n);
	return kolejka_bajtowa_daj_segment(k);
}

Segment* kolejka_bajtowa_daj_i_usun_dane(KolejkaBajtowa* k, size_t n){
	if(k == NULL){
		warn("kolejka_bajtowa_daj_i_usun_dane: kolejka NULL");
		return NULL;
	}
	
	if(kolejka_bajtowa_daj_liczbe_segmentow(k) == 0){
		kolejka_bajtowa_wrzuc_segment(k, segment_nowy(NULL, 0));
	}
	zrob_segment_rowny(k, n);
	return kolejka_bajtowa_daj_i_usun_segment(k);
}

size_t kolejka_bajtowa_usun_dane(KolejkaBajtowa* k, size_t n){
	if(k == NULL){
		warn("kolejka_bajtowa_usun_dane: kolejka NULL");
		return 0;
	}
	if(kolejka_bajtowa_daj_rozmiar(k) < n){
		warn("kolejka_bajtowa_usun_dane: usuwam wiecej niz jest w kolejce");
		n = kolejka_bajtowa_daj_rozmiar(k);
	}
	
	zrob_segment_rowny(k, n);
	
	Segment* s = kolejka_bajtowa_daj_i_usun_segment(k);
	if(s == NULL){
		return 0;
	}else{
		n = segment_daj_rozmiar(s);
		segment_usun(s);
		return n;
	}
}

size_t kolejka_bajtowa_daj_rozmiar(KolejkaBajtowa* k){
	if(k == NULL){
		warn("kolejka_bajtowa_daj_rozmiar: kolejka NULL");
		return 0;
	}
	return k->rozmiar;
}

bool kolejka_bajtowa_wypisz_sie(KolejkaBajtowa* k, int fd){
	for(
		Element* i = lista_daj_poczatek(k->segmenty);
		lista_czy_w_liscie(i);
		i = lista_daj_nastepny(i)
	){
		if(!segment_wypisz_sie(lista_dane_elementu(i), fd)){
			return false;
		}
	}
	return true;
}

/* prywatne */

size_t zrob_segment_wiekszy_rowny(KolejkaBajtowa* k, size_t n){
	if(lista_daj_poczatek(k->segmenty) == NULL){
		Segment* s = segment_nowy(NULL, 0);
		kolejka_bajtowa_wrzuc_segment(k, s);
		segment_usun(s);
		return 0;
	}
	
	while(
		segment_daj_rozmiar(lista_daj_dane_pierwszego_elementu(k->segmenty)) < n
	){
		Segment* a = lista_daj_dane_pierwszego_elementu_i_usun(k->segmenty);
		if(a == NULL){
			break;
		}
		Segment* b = lista_daj_dane_pierwszego_elementu_i_usun(k->segmenty);
		if(b == NULL){
			lista_dodaj_na_poczatku(a, k->segmenty);
			break;
		}
		segment_dopisz(a, b);
		segment_usun(b);
		lista_dodaj_na_poczatku(a, k->segmenty);
	}
	
	return segment_daj_rozmiar(lista_daj_dane_pierwszego_elementu(k->segmenty));
}

size_t zrob_segment_rowny(KolejkaBajtowa* k, size_t n){
	size_t nn = zrob_segment_wiekszy_rowny(k, n);
	if(nn < n){
		return nn;
	}
	
	Segment* s = lista_dane_elementu(lista_daj_poczatek(k->segmenty));
	Segment* ss = segment_utnij_poczatek(s, n);
	lista_dodaj_na_poczatku(ss, k->segmenty);
	return segment_daj_rozmiar(ss);
}
