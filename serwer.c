/* Jan Rydzewski 332461 */

#include "serwer.h"
#include <stdio.h>
#include "debug.h"
#include <stdlib.h>

int main(int argc, char** argv){
	char* p = "12461";
	char* F = "10560";
	char* L = "0";
	char* H = "10560";
	char* X = "10";
	char* i = "5";
	
	char c;
	opterr = 0;
	
	while((c = getopt (argc, argv, "p:F:L:H:X:i:")) != -1){
		switch(c){
			case 'p':
				p = optarg;
				break;
			case 'F':
				F = optarg;
				break;
			case 'L':
				L = optarg;
				break;
			case 'H':
				H = optarg;
				break;
			case 'X':
				X = optarg;
				break;
			case 'i':
				i = optarg;
				break;
			default:
				break;
		}
	}
	
	info("odpalam sie z opcjami:");
	info("port:                      %hu", (unsigned short)atoi(p));
	info("rozmiar FIFO:              %u", (unsigned int)atoi(F));
	info("niski stan FIFO:           %u", (unsigned int)atoi(L));
	info("wysoki stan FIFO:          %u", (unsigned int)atoi(H));
	info("rozmiar wynikowego bufora: %u", (unsigned int)atoi(X));
	info("odpalanie miksera co:      %u ms", (unsigned int)atoi(i));
	
	serwer_nowy(
		(unsigned short)atoi(p),
		(unsigned int)atoi(F),
		(unsigned int)atoi(L),
		(unsigned int)atoi(H),
		(unsigned int)atoi(X),
		(unsigned int)atoi(i)
	);
	
	spam("main: koniec");
	
	return 0;
}
