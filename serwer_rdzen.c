/* Jan Rydzewski 332461 */

#include <stdbool.h> /* bool */
#include <event2/event.h> /* libevent */
#include <stdio.h> /* printf */
#include "serwer.h"
#include <stdlib.h> /* malloc */
#include "debug.h"
#include <signal.h>

struct Serwer_{
	struct event_base* event_base;
	
	SerwerLaczyciel* laczyciel;
	SerwerUDP* UDP;
	SerwerMikser* mikser;
	SerwerWysylacz* wysylacz;
	
	Lista* klienci;
	unsigned int wolny_numer_klienta;
	unsigned int rozmiar_FIFO;
	unsigned int FIFO_pelne;
	unsigned int FIFO_puste;
	
	struct event* event_wyslij_raporty;
	struct event* event_sigint;
};

void wyslij_raporty(Serwer*);
void callback_wyslij_raporty(evutil_socket_t, short, void*);
void callback_sigint(evutil_socket_t, short, void*);

void serwer_nowy(
	unsigned short int port,
	unsigned int rozmiar_FIFO,
	unsigned int FIFO_pelne,
	unsigned int FIFO_puste,
	unsigned int rozmiar_wynikowego_bufora,
	unsigned int tx_interval
){
	
	spam("rdzen: robienie serwera");
	
	/* alloc */
	Serwer* serwer = calloc(1, sizeof(Serwer));
	if(serwer == NULL){
		erro("serwer: alloc");
		return;
	}
	
	
	/* event base */
	if(
		(serwer->event_base = event_base_new()) == NULL ||
		(serwer->laczyciel = serwer_laczyciel_nowy(serwer, port)) == NULL ||
		(serwer->UDP = serwer_UDP_nowy(serwer, port)) == NULL ||
		(serwer->mikser = serwer_mikser_nowy(serwer, tx_interval)) == NULL ||
		(serwer->wysylacz = serwer_wysylacz_nowy(serwer, rozmiar_wynikowego_bufora)) == NULL
	){
		erro("rdzen: nie udalo sie zrobic czesci");
		serwer_usun(serwer);
		return;
	}
	
	/* lista klientow */
	serwer->klienci = lista_nowa();
	if(serwer->klienci == NULL){
		erro("rdzen: nie udalo sie zrobic listy klientow");
		serwer_usun(serwer);
		return;
	}
	serwer->wolny_numer_klienta = 0;
	serwer->rozmiar_FIFO = rozmiar_FIFO;
	serwer->FIFO_puste = FIFO_puste;
	serwer->FIFO_pelne = FIFO_pelne;
	
	/* timer do wysylania raportów */
	serwer->event_wyslij_raporty = event_new(
		serwer_daj_event_base(serwer),
		-1,
		EV_PERSIST,
		callback_wyslij_raporty,
		(void*)serwer
	);
	struct timeval interval = { 0, (1000*1000)/CZESTOTLIWOSC_RAPORTU };
	if(
		serwer->event_wyslij_raporty == NULL ||
		event_add(serwer->event_wyslij_raporty, &interval) == -1
	){
		erro("serwer: nie dalo sie zrobic timera do raportow");
		serwer_usun(serwer);
		return;
	}
	
	/* handler siginta */
	serwer->event_sigint = evsignal_new(serwer->event_base, SIGINT, callback_sigint, (void*)serwer);
	if(
		serwer->event_sigint == NULL ||
		event_add(serwer->event_sigint, NULL) < 0
	){
		erro("serwer: nie dalo sie zrobic wychodzenia na SIGINT");
		serwer_usun(serwer);
		return;
	}
	
	spam("rdzen: serwer zrobiony, odpalam");
	
	event_base_dispatch(serwer->event_base);
	
	spam("rdzen: wyszedlem z eventloop, usuwam");
	
	serwer_usun(serwer);
}

/**
 * to sie musie ZAWSZE udać
 */
void serwer_usun(Serwer* serwer){
	spam("rdzen: usuwanie serwera");
	
	if(serwer != NULL){
		serwer_laczyciel_usun(serwer->laczyciel);
		serwer_UDP_usun(serwer->UDP);
		serwer_mikser_usun(serwer->mikser);
		serwer_wysylacz_usun(serwer->wysylacz);
		
		while(lista_daj_liczbe_elementow(serwer->klienci) > 0){
			serwer_klient_usun(lista_daj_dane_pierwszego_elementu_i_usun(serwer->klienci));
		}
		lista_usun(serwer->klienci);
		
		event_free(serwer->event_sigint);
		event_free(serwer->event_wyslij_raporty);
		event_base_free(serwer->event_base);
		
		free(serwer);
	}
	
	spam("rdzen: usuniety");
}

SerwerKlient* serwer_nowy_klient(Serwer* s, int fd){
	SerwerKlient* k = serwer_klient_nowy(
		s,
		fd,
		s->wolny_numer_klienta,
		s->rozmiar_FIFO,
		s->FIFO_puste,
		s->FIFO_pelne
	);
	s->wolny_numer_klienta++;
	if(lista_dodaj_na_koncu(k, s->klienci) == NULL){
		warn("rdzen: nie udalo sie nowego klienta dodac do listy");
		serwer_klient_usun(k);
		return NULL;
	}
	return k;
}

void serwer_usun_klienta(Serwer* s, SerwerKlient* k){
	bool usuniete = false;
	
	Element* e = lista_daj_poczatek(s->klienci);
	while(lista_czy_w_liscie(e)){
		SerwerKlient* kk = lista_dane_elementu(e);
		if(kk == k){
			if(usuniete){
				warn("rdzen: podwojne usuwanie klienta");
			}
			Element* ee = e;
			e = lista_daj_nastepny(e);
			lista_usun_element(ee);
			serwer_klient_usun(k);
			usuniete = true;
		}else{
			e = lista_daj_nastepny(e);
		}
	}
	
	if(!usuniete){
		warn("rdzen: nie moglem znalez klienta, nie usuwam");
	}
}

struct event_base* serwer_daj_event_base(Serwer* s){
	return s->event_base;
}

SerwerUDP* serwer_pozycz_UDP(Serwer* s){
	return s->UDP;
}

Lista* serwer_daj_liste_klientow(Serwer* s){
	return s->klienci;
}

SerwerMikser* serwer_pozycz_mikser(Serwer* s){
	return s->mikser;
}

SerwerWysylacz* serwer_pozycz_wysylacza(Serwer* s){
	return s->wysylacz;
}

/* prywatne */
void wyslij_raporty(Serwer* s){
	for(
		Element* e = lista_daj_poczatek(s->klienci);
		lista_czy_w_liscie(e);
		e = lista_daj_nastepny(e)
	){
		SerwerKlient* k = (SerwerKlient*)lista_dane_elementu(e);
		serwer_klient_wyslij_raport(k, "\n");
	}
	
	for(
		Element* e = lista_daj_poczatek(s->klienci);
		lista_czy_w_liscie(e);
		e = lista_daj_nastepny(e)
	){
		SerwerKlient* k = (SerwerKlient*)lista_dane_elementu(e);
		char* raport = serwer_klient_daj_raport(k);
		for(
			Element* ee = lista_daj_poczatek(s->klienci);
			lista_czy_w_liscie(ee);
			ee = lista_daj_nastepny(ee)
		){
			SerwerKlient* kk = (SerwerKlient*)lista_dane_elementu(ee);
			serwer_klient_wyslij_raport(kk, raport);
		}
		free(raport);
	}
}

void callback_wyslij_raporty(evutil_socket_t fd, short what, void* arg){
	(void)fd;
	(void)what;
	wyslij_raporty((Serwer*)arg);
}

void callback_sigint(evutil_socket_t fd, short what, void* arg){
	(void)fd;
	(void)what;
	Serwer* serwer = arg;
	event_base_loopexit(serwer->event_base, NULL);
}
