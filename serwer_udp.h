/* Jan Rydzewski 332461 */

#ifndef SERWER_UDP_H
#define SERWER_UDP_H

#include "serwer.h"

/**
 * SerwerUDP jest kowbojem UDP.
 * SerwerUDP odbiera dane z socketu i rozsyła do obiektów.
 * SerwerUDP może także wysyłać datagramy.
 */

SerwerUDP* serwer_UDP_nowy(Serwer*, unsigned short);
void serwer_UDP_usun(SerwerUDP*);

bool serwer_UDP_wyslij(SerwerUDP*, Datagram*, struct sockaddr_in6*);


#endif
