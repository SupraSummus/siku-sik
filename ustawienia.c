/* Jan Rydzewski 332461 */

#include "ustawienia.h"

/* wspolne */

/* serwer */
const int MAKSYMALNA_DLUGOSC_KOLEJKI_LISTEN = 10;
const int CZESTOTLIWOSC_RAPORTU = 1;

/* klient */
const int CZESTOTLIWOSC_KEEPALIVE = 10;
